// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME private
  public abstract class SendStream : Stream {
    internal SendStream (Connection connection, StreamType stream_type, string role_string, Gst.Element webrtc_bin, string? camera_id = null) {
      base (connection, stream_type, role_string, webrtc_bin, camera_id);
    }

    internal override async Json.Builder add_fields_for_start (Json.Builder b) {
      Json.Builder builder = yield base.add_fields_for_start (b);
      add_string_member (builder, "sdpOffer", yield this.create_answer_or_offer (true));

      return builder;
    }

    internal override void state_changed (Gst.State oldstate, Gst.State newstate, Gst.State pending) {
      if (oldstate == Gst.State.PAUSED && newstate == Gst.State.READY) {
        var builder = new Json.Builder ();
        builder.begin_object ();
        add_string_member (builder, "id", "stop");
        add_string_member (builder, "type", this.stream_type.to_string ());
        if (this is VideoShareStream) {
          add_string_member (builder, "cameraId", ((VideoShareStream) this).get_camera_id ());
        }
        add_string_member (builder, "role", this.role_string);
        builder.end_object ();
        this.connection.stream_manager.send_message (builder.get_root ());
      }
    }
  }

  // sealed
  private class VideoShareStream : SendStream {
    public string get_camera_id () {
      return this.connection.user_manager.current_user.id + "_" + this.device_id;
    }

    private string device_id;
    internal string camera_id;

    private const string pipeline_string = "pipewiresrc ! videoconvert ! queue ! " +
      "vp8enc deadline=1 keyframe-max-dist=2000 ! rtpvp8pay picture-id-mode=15-bit ! queue ! " +
      "application/x-rtp,media=video,encoding-name=VP8,payload=96 ! " +
      "webrtcbin name=webrtc_bin bundle-policy=max-bundle stun-server=stun://stun.l.google.com:19302";

    public VideoShareStream (Connection connection) {
      string device_id = "Db3jqfNLwyVTsK0uXMslnHRWJf+Sf9mTbR4PgiSZl8A=";
      string camera_id = connection.user_manager.current_user.id + "_" + device_id;
      var bin = (Gst.Bin) Gst.parse_bin_from_description (VideoShareStream.pipeline_string, false);
      base (connection, StreamType.VIDEO, "share", bin.get_by_name ("webrtc_bin"), camera_id);
      this.add (bin);
      this.device_id = device_id;
      this.camera_id = camera_id;
    }

    internal override async Json.Builder add_fields_for_start (Json.Builder b) {
      Json.Builder builder = yield base.add_fields_for_start (b);
      add_string_member (builder, "cameraId", this.get_camera_id ());
      builder.set_member_name ("bitrate");
      builder.add_int_value (200);
      builder.set_member_name ("record");
      builder.add_boolean_value (true);

      return builder;
    }

    internal override void on_play_start () {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.get_camera_id ());
      builder.end_array ();
      this.connection.connection.method.begin ("userShareWebcam", builder.get_root ());
    }
  }

  // sealed
  public class WebcamManager : Object {
    internal Connection connection;
    internal Gst.Pipeline pipeline = null;

    internal VideoShareStream stream_element;

    private bool _enabled = false;
    public bool enabled { get {
      return this._enabled;
      } set {
        if (value != this._enabled) {
          this._enabled = value;
          this.switch_audio.begin ();
        }
      }
    }

    private async void switch_audio () {
      if (this._enabled) {
        // FIXME just set the pipeline to NULL and PLAYING again instead of deleting it. (gstreamer warnings)
        this.stream_element = new VideoShareStream (this.connection);
        this.pipeline = new Gst.Pipeline ("pipeline");
        this.pipeline.add (this.stream_element);
        this.pipeline.call_async ((element) => {
          element.set_state (Gst.State.PLAYING);
        });
      } else {
        this.pipeline.call_async ((element) => {
          element.set_state (Gst.State.NULL);
        });
        this.stream_element = null;
        this.pipeline = null;
      }
    }

    internal WebcamManager (Connection connection) {
      this.connection = connection;
    }
  }

  private class FullAudioStream : SendStream {
    private const string pipeline_string = "audiotestsrc is-live=true wave=red-noise ! audioconvert ! volume name=microphone_mute ! audioresample ! queue ! " +
      "opusenc ! rtpopuspay ! queue ! application/x-rtp,media=audio,encoding-name=OPUS,payload=96 ! " +
      "webrtcbin name=webrtc_bin"; // bundle-policy=max-bundle stun-server=stun://stun.l.google.com:19302";

    public Gst.Element microphone_mute;
    public Gst.Element speaker_mute;
    internal Gst.Element decode_bin;

    public FullAudioStream (Connection connection) {
      var pipe = (Gst.Bin) Gst.parse_launch (FullAudioStream.pipeline_string);
      base (connection, StreamType.FULL_AUDIO, "sendrecv", pipe.get_by_name ("webrtc_bin"));
      this.microphone_mute = pipe.get_by_name ("microphone_mute");
      this.add (pipe);

      this.decode_bin = Gst.ElementFactory.make ("decodebin", "decode_bin");
      var queue = Gst.ElementFactory.make ("queue", "queue");
      var convert = Gst.ElementFactory.make ("audioconvert", "convert");
      var resample = Gst.ElementFactory.make ("audioresample", "resample");
      this.speaker_mute = Gst.ElementFactory.make ("volume", "volume");
      var sink = Gst.ElementFactory.make ("autoaudiosink", "sink");
      this.add (this.decode_bin);
      this.add (queue);
      this.add (convert);
      this.add (resample);
      this.add (this.speaker_mute);
      this.add (sink);
      queue.link (convert);
      convert.link (resample);
      resample.link (this.speaker_mute);
      this.speaker_mute.link (sink);
      this.webrtc_bin.pad_added.connect ((element, pad) => {
        print ("pad-added\n");
        if (pad.direction != Gst.PadDirection.SRC) {
          return;
        }
        this.webrtc_bin.link (this.decode_bin);
      });
      this.decode_bin.pad_added.connect ((element, pad) => {
        if (!pad.has_current_caps ()) {
          return;
        }
        Gst.Caps? caps = pad.get_current_caps ();
        if (caps == null || caps.get_size () < 1) {
          return;
        }
        unowned Gst.Structure str = caps.get_structure (0);
        print (str.get_name () + "\n");

        pad.link (queue.get_static_pad ("sink"));
      });
    }

    internal async override Json.Builder add_fields_for_start (Json.Builder b) {
      Json.Builder builder = yield base.add_fields_for_start (b);
      add_string_member (builder, "caleeName", this.connection.user_manager.current_user.id + "-bbbID-" + this.connection.user_manager.current_user.name);
      return builder;
    }

    internal override void connect () {
      base.connect ();
      this.connection.stream_manager.on_webrtc_audio_success["audio"].connect (this.on_webrtc_audio_success);
      this.connection.stream_manager.on_start_response["audio"].connect (this.on_start_response);
    }

    private void on_webrtc_audio_success () {
      var builder = new Json.Builder ();
      builder.begin_object ();
      add_string_member (builder, "id", "dtmf");
      add_string_member (builder, "type", "audio");
      add_string_member (builder, "tones", "1");
      builder.end_object ();
      this.connection.stream_manager.send_message (builder.get_root ());
    }

    internal override void disconnect () {
      base.disconnect ();
      this.connection.stream_manager.on_webrtc_audio_success.disconnect (this.on_webrtc_audio_success);
    }
  }
}
