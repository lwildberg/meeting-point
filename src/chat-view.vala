// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  [GtkTemplate (ui = "/org/gnome/gitlab/lwildberg/meeting-point/chat-view.ui")]
  public class ChatView : Adw.Bin {
    [GtkChild]
    private unowned Gtk.Box box;
    [GtkChild]
    private unowned Gtk.ListView list;
    [GtkChild]
    private unowned Gtk.Button delete_button;
    [GtkChild]
    private unowned Gtk.Entry message_entry;
    [GtkChild]
    private unowned Gtk.Button send_button;

    construct {
      this.connection = Application.get_default_connection ();
    }

    [GtkCallback]
    private void on_delete_button_clicked () {
      this.connection.chat_manager.clear_group_chat_history.begin ();
    }

    [GtkCallback]
    private void on_send_button_clicked () {
      this.chat.send_message.begin (this.message_entry.text);
      this.message_entry.text = "";
    }

    public MeetingPoint.Connection connection { get; private set; }
    public MeetingPoint.Chat chat { get; set; }
  }
}
