// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME sealed
  public class VideoStreamProvider : BaseManager {

    internal VideoStreamProvider (Connection connection) {
      base (connection, typeof (VideoStreamData), "video-streams", false);
      this.items_changed.connect (() => {
        if (this.get_n_items () == 1) {
          this.one_stream = (VideoStreamData?) this.get_item (0);
        } else {
          this.one_stream = null;
        }
      });
    }

    public VideoStreamData? one_stream { get; private set; }

    internal VideoStreamData? get_stream_with_camera_id (string id) {
      for (uint i = 0; i < this.get_n_items (); i++) {
        var stream_data = this.get_item (i) as VideoStreamData;
        if (id == stream_data.stream) {
          return stream_data;
        }
      }
      return null;
    }
  }

  // FIXME sealed
  public class VideoStreamData : BaseObject, Streamable, WebcamStreamable {

    internal string device_id;
    internal string stream;
    internal Gst.Pipeline pipeline = null;
    internal ViewerVideoStream stream_element = null;
    public Gdk.Paintable stream_paintable { get; protected set; default = null; }

    public User user { get; protected set; }
    public bool floor { get; private set; default = false; } // ????
    public DateTime last_floor { get; private set; } // ????

    internal VideoStreamData () {}

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      if (fields.has_member ("deviceId")) {
        this.device_id = fields.get_string_member ("deviceId");
      }
      if (fields.has_member ("userId")) {
        this.user = this.connection.user_manager.get_user_with_id (fields.get_string_member ("userId"));
      }
      if (fields.has_member ("floor")) {
        this.floor = fields.get_boolean_member ("floor");
      }
      if (fields.has_member ("lastFloorTime")) {
        this.last_floor = convert_time (fields.get_int_member ("lastFloorTime"));
      }
      if (fields.has_member ("stream")) {
        this.stream = fields.get_string_member ("stream");
      }
      // if (cleared.foreach_element () ... ????

      this.user.stream = this;

      if (this.stream_element == null) {
        this.stream_element = new ViewerVideoStream (this.connection, this.stream);
        this.stream_element.paintable_sink.notify.connect ((pspec) => {
          print ("pspec: %s\n", pspec.name);
          Gdk.Paintable p;
          this.stream_element.paintable_sink.get ("paintable", out p);
          this.stream_paintable = p;
        });
        Gdk.Paintable p;
        this.stream_element.paintable_sink.get ("paintable", out p);
        this.stream_paintable = p;
        // bind_property ("paintable", this, "stream-paintable");

        if (this.pipeline == null) {
          this.pipeline = new Gst.Pipeline ("pipeline");
          this.pipeline.add (this.stream_element);
          this.pipeline.set_state (Gst.State.PLAYING);
        }
      }

      return false;
    }

    ~VideoStreamData () {
      this.user.stream = null;
      this.stream_element.disconnect ();
    }
  }

  public interface Streamable : Object {
    public abstract Gdk.Paintable stream_paintable { get; protected set; }

    internal void _set_stream_paintable (Gdk.Paintable p) {
      this.stream_paintable = p;
    }
  }

  public interface WebcamStreamable : Object, Streamable {
    public abstract User user { get; protected set; }
  }
}
