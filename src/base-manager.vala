// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  public abstract class BaseObject : Object {

    internal string ddp_id;
    internal weak Connection connection;
    internal CompareDataFunc<BaseObject> sort_func;

    internal virtual void init () { }

    /* returns false to indicate that the list needs to be sorted again */
    internal abstract bool update (Json.Object fields, Json.Array? cleared = null);

    internal BaseObject () {}
  }

  public abstract class BaseManager : Object, ListModel {

    internal weak Connection connection;
    internal ListStore items;
    private bool sort;
    private Type item_type;
    private string collection;

    internal BaseManager (Connection connection, Type item_type, string collection, bool sort) {
      this.connection = connection;
      this.sort = sort;
      this.item_type = item_type;
      this.items = new ListStore (item_type);
      this.collection = collection;


      this.items.items_changed.connect ((position, removed, added) => {
        this.items_changed (position, removed, added);
      });

      this.connection.connection.on_added.connect ((c, id, fields) => {
        if (c == this.collection) {
          BaseObject item;

          item = (BaseObject) Object.new (this.item_type);
          item.ddp_id = id;
          item.connection = this.connection;
          item.init ();
          item.update (fields);
          if (this.sort) {
            this.items.insert_sorted (item, item.sort_func);
          } else {
            this.items.append (item);
          }
        }
      });

      this.connection.connection.on_changed.connect ((c, id, fields, cleared) => {
        if (c != this.collection) {
          return;
        }
        uint position = this.get_position_with_ddp_id (id);
        var item = this.get_item (position) as BaseObject;
        if (item.update (fields, cleared) && this.sort) {
          this.items.remove (position);
          this.items.insert_sorted (item, item.sort_func);
        }
      });

      this.connection.connection.on_removed.connect ((c, id) => {
        if (this.collection != c) {
          return;
        }
        uint position = this.get_position_with_ddp_id (id);
        this.items.remove (position);
      });
    }

    private uint? get_position_with_ddp_id (string ddp_id) {
      for (uint i = 0; i < this.items.get_n_items (); i++) {
        var item = this.items.get_item (i) as BaseObject;
        if (item.ddp_id == ddp_id) {
          return i;
        }
      }
      return null;
    }

    internal BaseObject? get_item_with_ddp_id (string ddp_id) {
      return (BaseObject) this.get_item (this.get_position_with_ddp_id (ddp_id));
    }

    internal async void subscribe () throws MeetingError {
      yield this.connection.connection.sub (this.collection);
    }

    public new uint get_n_items () {
      return this.items.get_n_items ();
    }

    public new Type get_item_type () {
      return this.item_type;
    }

    public new Object? get_item (uint position) {
      return this.items.get_item (position);
    }
  }
}
