// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {

  public enum GuestStatus {
    ALLOW,
    DENY
  }

  public enum ClientType {
    HTML5,
    DIAL_IN
  }

  // FIXME sealed
  public class User : BaseObject {
    public string id { get; private set; }
    public string extern_id { get; private set;}
    public bool is_moderator { get; private set;  default = false; }
    public DateTime login_time { get; private set; }
    public bool is_breakout { get; private set; } // meeting
    public int64 breakout_parent_id { get; private set; } // meeting
    public Gdk.RGBA color { get; private set; default=Gdk.RGBA (); }
    public Emoji status { get; private set; }
    public DateTime emoji_time  { get; private set; }
    public bool guest { get; private set; } //?
    public GuestStatus guest_status { get; private set; } //?
    public bool locked { get; private set; } //?
    public bool logged_out { get; private set; }
    public bool mobile { get; private set; }
    public string name { get; private set; }
    public bool presenter { get; private set; }
    public bool inactivity_check { get; private set; } //?
    public ClientType client_type { get; private set; } //?
    public bool validated { get; private set; } //?
    public bool approved { get; private set; } //?
    public bool authed { get; private set; } //?
    public string avatar { get; private set; } //?
    public string sort_name { get; private set; } //?
    public string effective_connection_type { get; private set; } //?
    public int64 response_delay { get; private set; } //?
    public DateTime auth_token_validated_time { get; private set; }

    public weak Chat? chat { get; internal set; default = null; }

    public weak VideoStreamData? stream { get; internal set; default = null; }

    internal Json.Object fields = new Json.Object ();

    internal User () {}
    internal User.already_left (string name, string role) {
      this.name = name;
      this.is_moderator = (role == "MODERATOR");
    }

    construct {
      this.sort_func = (a, b) => {
        return strcmp (((User) a).name, ((User) b).name);
      };
    }

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      bool changed = false;
      if (fields.has_member ("userId")) {
        this.id = fields.get_string_member ("userId");
        if (this.id == this.connection.intern_id) {
          this.connection.user_manager.current_user = this;
        }
      }
      if (fields.has_member ("externUserId")) {
        this.extern_id = fields.get_string_member ("externUserId");
      }
      if (fields.has_member ("role")) {
        this.is_moderator = (fields.get_string_member ("role") == "MODERATOR");
      }
      // meeting_id + breakout parent id
      if (fields.has_member ("loginTime")) {
        this.login_time = convert_time (fields.get_int_member ("loginTime"));
      }
      if (fields.has_member ("color")) {
        this.color.parse (fields.get_string_member ("color"));
      }
      //this.emoji_time = convert_time (fields.get_int_member ("emojiTime")); //------------------------
      if (fields.has_member ("emoji")) {
        this.status = Emoji.from_string (fields.get_string_member ("emoji"));
      }
      if (fields.has_member ("guest")) {
        this.guest = fields.get_boolean_member ("guest");
      }
      if (fields.has_member ("guestStatus")) {
        this.guest_status = (fields.get_string_member ("guestStatus") == "ALLOW") ? GuestStatus.ALLOW : GuestStatus.DENY;
      }
      if (fields.has_member ("locked")) {
        this.locked = fields.get_boolean_member ("locked");
      }
      if (fields.has_member ("loggedOut")) {
        this.logged_out = fields.get_boolean_member ("loggedOut");
      }
      if (fields.has_member ("mobile")) {
        this.mobile = fields.get_boolean_member ("mobile");
      }
      if (fields.has_member ("presenter")) {
        this.presenter = fields.get_boolean_member ("presenter");
      }
      if (fields.has_member ("name")) {
        this.name = fields.get_string_member ("name");
        changed = true;
      }
      if (fields.has_member ("inactivityCheck")) {
        this.inactivity_check = fields.get_boolean_member ("inactivityCheck");
      }
      if (fields.has_member ("clentType")) {
        this.client_type = (fields.get_string_member ("clientType") == "HTML5") ? ClientType.HTML5 : ClientType.DIAL_IN;
      }
      if (fields.has_member ("validated")) {
        this.validated = fields.get_boolean_member ("validated");
      }
      if (fields.has_member ("approved")) {
        this.approved = fields.get_boolean_member ("approved");
      }
      if (fields.has_member ("authed")) {
        this.authed = fields.get_boolean_member ("authed");
      }
      if (fields.has_member ("avatar")) {
        this.avatar = ""; //
      }
      if (fields.has_member ("sortName")) {
        this.sort_name = fields.get_string_member ("sortName");
      }
      if (fields.has_member ("effectiveConnectionType")) {
        this.effective_connection_type = ""; //
      }
      if (fields.has_member ("responseDelay")) {
        this.response_delay = fields.get_int_member ("responseDelay");
      }
      if (fields.has_member ("authTokenValidatedTime")) {
        this.auth_token_validated_time = convert_time (fields.get_int_member ("authTokenValidatedTime"));
      }

      if (cleared == null) {
        return changed;
      }
      // when does that happen ?

      fields.foreach_member ((object, name, node) => {
        this.fields.set_member (name, node);
      });
      cleared.foreach_element ((arra, index, node) => {
        this.fields.remove_member (cleared.get_string_element (index));
      });
      return changed;
    }

    public bool equals (User user) {
      if (this.id == user.id) {
        return true;
      } else {
        return false;
      }
    }

    public async void set_emoji_state (Emoji state) throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.id);
      builder.add_string_value (state.to_string ());
      builder.end_array ();

      yield this.connection.connection.method ("setEmojiStatus", builder.get_root ());
    }

    public async void remove (bool ban = false) throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.id);
      builder.add_boolean_value (ban);
      builder.end_array ();

      yield this.connection.connection.method ("removeUser", builder.get_root ());
    }

    // role changed to attendee if to_moderator=false
    public async void change_role (bool to_moderator) throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.id);
      builder.add_string_value (to_moderator ? "MODERATOR" : "ATTENDEE");
      builder.end_array ();

      yield this.connection.connection.method ("changeRole", builder.get_root ());
    }

    public async void assign_presenter () throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.id);
      builder.end_array ();

      yield this.connection.connection.method ("assignPresenter", builder.get_root ());
    }
  }
}
