// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  public class Screenshare : BaseObject, Streamable {

    public string stream_id { get; private set; }
    public int64 video_width { get; private set; } // ?
    public int64 video_height { get; private set; } // ?
    public DateTime timestamp { get; private set; }
    public bool has_audio { get; set; }
    internal Gst.Pipeline pipeline = null;
    internal ScreenshareStream stream_element = null;

    public Gdk.Paintable stream_paintable { get; protected set; default = null; }

    internal Screenshare () {}

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      Json.Object screenshare_fields = fields.get_object_member ("screenshare");
      if (screenshare_fields.has_member ("stream")) {
        this.stream_id = screenshare_fields.get_string_member ("stream");
      }
      if (screenshare_fields.has_member ("vidWidth")) {
        this.video_width = screenshare_fields.get_int_member ("vidWidth");
      }
      if (screenshare_fields.has_member ("vidHeight")) {
        this.video_height = screenshare_fields.get_int_member ("vidHeight");
      }
      if (screenshare_fields.has_member ("timestamp")) {
        this.timestamp = new DateTime.from_unix_local(int64.parse (screenshare_fields.get_string_member ("timestamp")) / 1000);
      }
      if (screenshare_fields.has_member ("hasAudio")) {
        this.has_audio = screenshare_fields.get_boolean_member ("hasAudio");
      }

      if (this.stream_element == null) {
        this.stream_element = new ScreenshareStream (this.connection, this.stream_id);
        this.stream_element.paintable_sink.notify.connect ((pspec) => {
          print ("pspec: %s\n", pspec.name);
          Gdk.Paintable p;
          this.stream_element.paintable_sink.get ("paintable", out p);
          this.stream_paintable = p;
          this.connection.screenshare_manager.current = this;
        });
        Gdk.Paintable p;
        this.stream_element.paintable_sink.get ("paintable", out p);
        this.stream_paintable = p;
        this.connection.screenshare_manager.current = this;
        // bind_property ("paintable", this, "stream-paintable");

        if (this.pipeline == null) {
          this.pipeline = new Gst.Pipeline ("pipeline");
          this.pipeline.add (this.stream_element);
          this.pipeline.set_state (Gst.State.PLAYING);
        }
      }

      return false;
    }

    ~Screenshare () {
      this.stream_element.disconnect ();
    }
  }

  public class ScreenshareManager : BaseManager {
    internal ScreenshareManager (Connection connection) {
      base (connection, typeof (Screenshare), "screenshare", false);
    }

    internal Screenshare? get_stream_from_id (string stream_id) {
      for (uint i = 0; i < this.get_n_items (); i++) {
        var stream_data = this.get_item (i) as Screenshare;
        if (stream_id == stream_data.stream_id) {
          return stream_data;
        }
      }
      return null;
    }

    public Screenshare current { get; internal set; }
  }

  private class ScreenshareStream : RecieveStream {
    internal string stream_id;
    internal Gst.Element paintable_sink;

    internal ScreenshareStream (Connection connection, string stream_id) {
      base (connection, StreamType.SCREEN_SHARE, "recv");
      print (stream_id);
      this.stream_id = stream_id;

      var queue = Gst.ElementFactory.make ("queue", "queue");
      var convert = Gst.ElementFactory.make ("videoconvert", "convert");
      this.paintable_sink = Gst.ElementFactory.make ("gtk4paintablesink", "paintable_sink");
      this.add (queue);
      this.add (convert);
      this.add (this.paintable_sink);
      queue.link (convert);
      convert.link (this.paintable_sink);

      this.decode_bin.pad_added.connect ((element, pad) => {
        if (!pad.has_current_caps ()) {
          return;
        }
        Gst.Caps? caps = pad.get_current_caps ();
        if (caps == null || caps.get_size () < 1) {
          return;
        }
        unowned Gst.Structure str = caps.get_structure (0);
        print (str.get_name () + "\n");

        pad.link (queue.get_static_pad ("sink"));
      });
    }

    internal override async Json.Builder add_fields_for_start (Json.Builder b) {
      Json.Builder builder = yield base.add_fields_for_start (b);

      add_string_member (builder, "callerName", this.connection.user_manager.current_user.id);
      builder.set_member_name("bitrate");
      builder.add_int_value (100);
      builder.set_member_name ("hasAudio");
      builder.add_boolean_value (this.connection.screenshare_manager.get_stream_from_id (this.stream_id).has_audio);
      return builder;
    }

    internal override void add_fields_for_subscriber_answer (ref Json.Builder builder) {
      base.add_fields_for_subscriber_answer (ref builder);

      add_string_member (builder, "callerName", this.connection.user_manager.current_user.id);
    }
  }
}
