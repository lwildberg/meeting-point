// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  private class SenfcallLauncherImplementation : LauncherImplementation {
    public SenfcallLauncherImplementation (Launcher launcher) {
      base (launcher, typeof (SenfcallLauncherImplementation));

      // this.launcher.notify["link"].connect ();
      // this.launcher.notify["room-name"].connect ();
    }

    public override async bool start_meeting () throws MeetingError {
      // room name (and password)
      string address = Uri.join (UriFlags.NONE, "https", null, "public.senfcall.de", 443, "/en", null, null);
      string form = Soup.Form.encode ("room", this.launcher.room_name, "room_password", this.launcher.password);
      Soup.Message message = new Soup.Message.from_encoded_form ("POST", address, form);
      message.add_flags (Soup.MessageFlags.NO_REDIRECT);
      bool error = false;
      try {
        yield this.launcher.session.send_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("couldn't setup the meeting room");
      }
      Soup.MessageHeaders response = message.response_headers;
      string location = response.get_one ("Location");
      print (location.split ("/")[2]+"\n");

      if (location.contains ("roomBlocked=true")) {
        return false;
      }

      return true;
    }

    private string get_room_name_from_link () {
      Uri? uri = null;
      try {
        uri = Uri.parse (this.launcher.link, UriFlags.NONE);
      } catch (Error e) {
        assert_not_reached ();
      }

      return uri.get_path ().replace ("/", "");
    }

    public override async Connection join_meeting () throws MeetingError {
      string address = Uri.join (UriFlags.NONE, "https", null, "public.senfcall.de", 443, "/en/easy-" + get_room_name_from_link (), null, null);
      string form = Soup.Form.encode ("name", this.launcher.username, "room_password", this.launcher.password);
      var message = new Soup.Message.from_encoded_form ("POST", address, form);
      //message.add_flags (Soup.MessageFlags.NO_REDIRECT);
      Bytes? data = null;
      bool error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("couldn't setup the name");
      }
      if (message.uri.get_path () == "/en") {
        throw new MeetingError.NO_ROOM ("this room does not exist yet");
      }

      return yield this.finish_join (message.uri, data);
    }

    public override async bool room_exists_already () throws MeetingError {
      string address = Uri.join (UriFlags.NONE, "https", null, "public.senfcall.de", 443, "/en/" + get_room_name_from_link (), null, null);
      var message = new Soup.Message ("GET", address);
      message.add_flags (Soup.MessageFlags.NO_REDIRECT);
      Bytes? data = null;
      bool error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("server error; could not check for existing room.");
      }

      Soup.MessageHeaders response = message.response_headers;
      string location = response.get_one ("Location");
      if (location != null && location.contains ("roomMissing=true")) {
        return false;
      }
      return true;
    }
  }
}
