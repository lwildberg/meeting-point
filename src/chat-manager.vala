// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME sealed
  public class ChatManager : BaseManager {

    public Chat room_chat { get; internal set; }

    private int64 items_per_page;
    private const TimeSpan TYPING_LENGTH = TimeSpan.SECOND * 1; // milliseconds

    internal ChatManager (Connection connection) {
      base (connection, typeof (Chat), "group-chat", true);
      this.items_per_page = this.connection.meteor_settings.get_object_member ("chat").get_int_member ("itemsPerPage");
    }

    public async void clear_group_chat_history () throws MeetingError {
      yield this.connection.connection.method ("clearPublicChatHistory");
    }

    internal async void get_messages_before_join () throws MeetingError {
      Json.Node? result = null;
      result = yield this.connection.connection.method ("chatMessageBeforeJoinCounter");
      assert (result != null);
      result.get_array ().foreach_element ((array, index, node) => {
        Json.Object object = node.get_object ();
        Chat? chat = this.get_chat_with_id (object.get_string_member ("chatId"));
        assert (chat != null);
        chat.set_messages_before_join ((uint) object.get_int_member ("count"));
      });
    }

    public async void create_chat (User user) throws MeetingError {
      Json.Object? user_data = null;
      string? document_id = null;
      uint position;
      this.connection.user_manager.items.find (user, out position);
      for (position = 0; position < this.connection.user_manager.get_n_items (); position++) {
        User u = (User) this.connection.user_manager.get_item (position);
        if (u.id == user.id) {
          user_data = u.fields;
          document_id = u.ddp_id;
          break;
        }
      }
      assert (user_data != null && document_id != null);

      user_data.set_string_member ("_id", document_id);
      var param = new Json.Array ();
      param.add_object_element (user_data);

      var node = new Json.Node.alloc ();
      node.set_array (param);
      yield this.connection.connection.method ("createGroupChat", node);

      user_data.remove_member ("_id");
    }

    internal Chat? get_chat_with_id (string id) {
      for (int i = 0; i < this.items.get_n_items (); i++) {
        Chat chat = (Chat) this.items.get_item (i);
        if (chat.id == id) {
          return chat;
        }
      }
      return null;
    }

    //TODO destroyGroupChat ????
  }
}
