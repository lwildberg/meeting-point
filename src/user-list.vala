// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  [GtkTemplate (ui = "/org/gnome/gitlab/lwildberg/meeting-point/user-list.ui")]
  public class UserList : Adw.Bin {
    [GtkChild]
    private unowned Gtk.Box box;
    [GtkChild]
    private unowned Gtk.ListView list;

    construct {
      this.connection = Application.get_default_connection ();
    }

    public MeetingPoint.Connection connection { get; private set; }
  }
}
