// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  private class UsersTypingManager : BaseManager {
    private DdpConnection.Subscription user_typing_sub;

    public UsersTypingManager (Connection connection) {
      base (connection, typeof(UserTyping), "users-typing", false);
    }
  }

  private class UserTyping : BaseObject {
    public Chat chat { get; private set; }
    public User user { get; private set; }
    public DateTime time { get; private set; }

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      if (fields.has_member ("isTypingTo")) {
        string chat_id = fields.get_string_member ("isTypingTo");
        if (chat_id == "public") {
          this.chat = this.connection.chat_manager.room_chat;
        } else {
          this.chat = this.connection.chat_manager.get_chat_with_id (chat_id);
        }
      }
      if (fields.has_member ("userId")) {
        this.user = this.connection.user_manager.get_user_with_id (fields.get_string_member ("userId"));
      }
      if (!fields.has_member ("time")) {
        this.time = convert_time (fields.get_object_member ("time").get_int_member ("$date"));
      }
      return false;
    }
  }
}
