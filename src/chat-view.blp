// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

using Gtk 4.0;
using Adw 1;

template $MeetingPointChatView : Adw.Bin {
  Box box {
    orientation: vertical;

    Box {
      Button {
        label: "Group Chat:";
        sensitive: false;
        halign: start;

        styles ["flat"]
      }

      Button delete_button {
        icon-name: "user-trash-symbolic";
        visible: bind template.connection as <$MeetingPointConnection>.user-manager as <$MeetingPointUserManager>.current-user as <$MeetingPointUser>.is_moderator;
        halign: end;
        hexpand: true;
        clicked => $on_delete_button_clicked();
      }

      styles ["toolbar"]
    }

    Separator {}

    ScrolledWindow {
      ListView list {
        vexpand: true;
        hexpand: true;
        model: NoSelection {
          model: bind template.chat;
        };
        factory: BuilderListItemFactory {
          resource: "/org/gnome/gitlab/lwildberg/meeting-point/chat-message-item.ui";
        };

        styles ["navigation-sidebar"]
      }
    }

    Separator {}

    Box {
      orientation: horizontal;

      Entry message_entry {
        hexpand: true;
      }

      Button send_button {
        icon-name: "paper-plane-symbolic";
        valign: center;
        clicked => $on_send_button_clicked();
        styles ["circular", "suggested-action"]
      }

      styles ["toolbar"]
    }
  }
}