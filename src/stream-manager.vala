// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {

  // FIXME private
  public enum StreamType {
    VIDEO,
    AUDIO,
    FULL_AUDIO,
    SCREEN_SHARE;

    public static StreamType from_string (string type) {
      switch (type) {
        case "video":
          return VIDEO;
        case "audio":
          return AUDIO;
        case "fullaudio":
          return FULL_AUDIO;
        case "screenshare":
          return SCREEN_SHARE;
      }
      critical ("StreamType not recognized: %s", type);
      return -1;
    }

    public string to_string () {
      switch (this) {
        case VIDEO:
          return "video";
        case AUDIO:
          return "audio";
        case FULL_AUDIO:
          return "fullaudio";
        case SCREEN_SHARE:
          return "screenshare";
      }
      assert_not_reached ();
    }
  }

  // FIXME sealed
  private class StreamManager : Object {

    private Soup.WebsocketConnection? socket = null;

    private weak Connection connection;

    private const string STREAM_SOCKET_PATH = "/bbb-webrtc-sfu";

    internal StreamManager (Connection connection) {
      this.connection = connection;
    }

    public new async void connect () throws MeetingError {
      var message = new Soup.Message ("GET", Uri.join (
        UriFlags.NONE, "wss", null, this.connection.connection.host, this.connection.connection.port, STREAM_SOCKET_PATH, @"sessionToken=$(this.connection.session_token)", null
      ));
      bool error = false;
      try {
        this.socket = yield this.connection.session.websocket_connect_async (message, null, null, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.AUTHENTICATED ("not authenticated");
      }

      this.socket.message.connect ((t, message) => {
        debug ("Message on webrtc-sfu arrived:\n"+ (string) message.get_data () + "\n");
        Json.Node node;
        try {
          node = Json.from_string ((string) message.get_data ());
        } catch (Error e) {
          critical (@"Error occurred in Stream-socket: $(e.message)\n");
          return;
        }
        Json.Object obj = node.get_object ();

        string detail = obj.has_member ("type") ? obj.get_string_member ("type") : "";
        if (obj.has_member ("cameraId")) {
          detail += "-" + obj.get_string_member ("cameraId");
        }

        string message_id = obj.get_string_member ("id");
        switch (message_id) {
          case "pong":
            Idle.add (this.ping_callback);
            break;
          case "startResponse":
            this.on_start_response[detail] (obj);
            break;
          case "playStart":
            this.on_play_start[detail] (obj);
            break;
          case "webRTCAudioSuccess":
            this.on_webrtc_audio_success[detail] (obj);
            break;
          case "playStop":
            this.on_play_stop[detail] (obj);
            break;
          case "iceCandidate":
            this.on_ice_candidate[detail] (obj);
            break;
          case "error":
            critical ("Recieved error on webrtc-sfu.\n");
            break;
        }
      });
    }

    private SourceFunc ping_callback;

    public async void ping () {
      var builder = new Json.Builder ();
      builder.begin_object ();
      builder.set_member_name ("id");
      builder.add_string_value ("ping");
      builder.end_object ();
      this.send_message (builder.get_root ());
      this.ping_callback = ping.callback;
      yield;
    }

    public void send_message (Json.Node message) {
      var generator = new Json.Generator ();
      generator.set_root (message);
      string msg = generator.to_data (null);
      debug ("send webrtc-sfu message:\n" + msg + "\n");
      this.socket.send_text (msg);
    }

    [Signal (detailed = true)]
    public signal void on_start_response (Json.Object object);
    [Signal (detailed = true)]
    public signal void on_play_start (Json.Object object);
    [Signal (detailed = true)]
    public signal void on_webrtc_audio_success (Json.Object object);
    [Signal (detailed = true)]
    public signal void on_play_stop (Json.Object object);
    [Signal (detailed = true)]
    public signal void on_ice_candidate (Json.Object object);
  }
}

