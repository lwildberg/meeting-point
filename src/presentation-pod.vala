// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  //fixme sealed
  public class PresentationPodManager : BaseManager {
    internal PresentationPodManager (Connection connection) {
      base (connection, typeof (PresentationPod), "presentation-pods", false);
    }

    internal PresentationPod? get_pod_with_id (string id) {
      for (int i = 0; i < this.items.get_n_items (); i++) {
        PresentationPod pod = (PresentationPod) this.items.get_item (i);
        if (pod.id == id) {
          return pod;
        }
      }
      return null;
    }
  }

  //fixme sealed
  public class PresentationPod : BaseObject {
    public string id { get; private set; }
    public User current_presenter { get; private set; }

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      if (fields.has_member ("podId")) {
        this.id = fields.get_string_member ("podId");
      }
      if (fields.has_member ("currentPresenterId")) {
        this.current_presenter = this.connection.user_manager.get_user_with_id (fields.get_string_member ("currentPresenterId"));
      }
      return false;
    }
  }
}
