// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  //fixme sealed
  public class PresentationManager : BaseManager {
    internal PresentationManager (Connection connection) {
      base (connection, typeof (Presentation), "presentations", false);
      // FIXME
      // this.connection.slides_manager.notify.connect (() => {
      //   this.stream_paintable = this.connection.slides_manager.current_slide.texture;
      // });
    }

    internal Presentation? get_presentation_with_id (string id) {
      for (int i = 0; i < this.items.get_n_items (); i++) {
        Presentation presentation = (Presentation) this.items.get_item (i);
        if (presentation.id == id) {
          return presentation;
        }
      }
      return null;
    }
  }

  //fixme sealed
  public class Presentation : BaseObject {
    public string id { get; private set; }
    public PresentationPod pod { get; private set; }
    public string name { get; private set; }
    public bool conversion_done { get; private set; }
    public bool conversion_error { get; private set; }
    public string conversion_status { get; private set; } // TODO enum
    public int64 n_pages { get; private set; }
    public int64 completed_pages { get; private set; }
    public bool downloadable { get; private set; }
    public bool removable { get; private set; }
    // meeting_id ?
    // public string temporary_id { get; private set; } // ?
    // public string thumb_uri { get; private set; }

    internal Presentation () {}

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      if (fields.has_member ("id")) {
        this.id = fields.get_string_member ("id");
      }
      if (fields.has_member ("podId")) {
        this.pod = this.connection.presentation_pod_manager.get_pod_with_id (fields.get_string_member ("podId"));
      }
      if (fields.has_member ("conversion")) {
        Json.Object f = fields.get_object_member ("conversion");
        if (f.has_member ("done")) {
          this.conversion_done = f.get_boolean_member ("done");
        }
        if (f.has_member ("error")) {
          this.conversion_error = f.get_boolean_member ("error");
        }
        if (f.has_member ("status")) {
          this.conversion_status = f.get_string_member ("status");
        }
        if (f.has_member ("numPages")) {
          this.n_pages = f.get_int_member ("numPages");
        }
        if (f.has_member ("pagesCompleted")) {
          this.completed_pages = f.get_int_member ("pagesCompleted");
        }
      }
      if (fields.has_member ("name")) {
        this.name = fields.get_string_member ("name");
      }
      return false;
    }
  }
}
