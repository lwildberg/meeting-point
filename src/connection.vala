// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {

  public errordomain MeetingError {
    CONNECT,
    CREDENTIALS,
    AUTHENTICATED,
    CONNECTION,
    NO_CONNECTION,
    NO_ROOM
  }

  // FIXME sealed
  public class Connection : GLib.Object {

    internal string host;
    internal string meeting_id;
    internal string extern_id;
    internal string intern_id;
    internal string auth_token;
    internal string session_token;

    internal Soup.Session session;

    public UserManager user_manager { get; private set; }
    public ChatManager chat_manager { get; private set; }
    public VideoStreamProvider stream_provider { get; private set; }
    public MeetingManager meeting_manager { get; private set; }
    public AudioManager audio_manager { get; private set; }
    public WebcamManager webcam_manager { get; private set; }
    public ScreenshareManager screenshare_manager { get; private set; }
    public PresentationManager presentation_manager { get; private set; }
    public PresentationPodManager presentation_pod_manager { get; private set; }
    public SlidesManager slides_manager { get; private set; }

    internal ChatMessageManager chat_message_manager;
    internal UsersTypingManager users_typing_manager;
    internal StreamManager stream_manager;
    internal Json.Object meteor_settings;
    internal DdpConnection connection;

    internal Connection (Soup.Session session, string host, string meeting_id, string extern_id, string intern_id, string auth_token, string session_token, Json.Object settings) {
      this.session = session;
      this.host = host;
      this.meeting_id = meeting_id;
      this.extern_id = extern_id;
      this.intern_id = intern_id;
      this.auth_token = auth_token;
      this.session_token = session_token;
      this.meteor_settings = settings;

      this.connection = new DdpConnection (this.host, "/html5client", 443);

      this.user_manager = new UserManager (this, this.extern_id);
      this.meeting_manager = new MeetingManager (this);
      this.chat_manager = new ChatManager (this);
      this.stream_provider = new VideoStreamProvider (this);
      this.stream_manager = new StreamManager (this);
      this.chat_message_manager = new ChatMessageManager (this);
      this.users_typing_manager = new UsersTypingManager (this);
      this.audio_manager = new AudioManager (this);
      this.webcam_manager = new WebcamManager (this);
      this.screenshare_manager = new ScreenshareManager (this);
      this.presentation_manager = new PresentationManager (this);
      this.presentation_pod_manager = new PresentationPodManager (this);
      this.slides_manager = new SlidesManager (this);
    }

    public async void init () throws MeetingError {
      if (this.connection.connection_state != CONNECTED) {
        yield this.connection.connect (this.session);
      }

      yield this.validate_auth_token ();
      yield this.meeting_manager.subscribe ();
      yield this.user_manager.subscribe ();
      yield this.chat_manager.subscribe ();
      yield this.users_typing_manager.subscribe ();
      yield this.chat_manager.get_messages_before_join ();
      yield this.stream_manager.connect ();
      yield this.stream_provider.subscribe ();
      this.audio_manager.init ();
      yield this.screenshare_manager.subscribe ();
      yield this.presentation_pod_manager.subscribe ();
      yield this.presentation_manager.subscribe ();
      yield this.slides_manager.subscribe ();
    }

    private async void validate_auth_token () throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.meeting_id);
      builder.add_string_value (this.intern_id);
      builder.add_string_value (this.auth_token);
      builder.add_string_value (this.extern_id);
      builder.end_array ();

      yield this.connection.method ("validateAuthToken", builder.get_root ());
    }
  }

  private DateTime convert_time (int64 time) {
    return new DateTime.from_unix_local (time / 1000);
  }
}

