// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  [GtkTemplate (ui = "/org/gnome/gitlab/lwildberg/meeting-point/launcher-window.ui")]
  public class LauncherWindow : Adw.ApplicationWindow {
    [GtkChild]
    private unowned Gtk.Button start_button;
    [GtkChild]
    private unowned Adw.EntryRow address_entry;
    [GtkChild]
    private unowned Adw.EntryRow username_entry;
    [GtkChild]
    private unowned Adw.EntryRow password_entry;

    public MeetingPoint.Launcher launcher { get; set; }

    private bool address_valid = false;
    private bool username_valid = false;

    public LauncherWindow (Gtk.Application app) {
      Object (application: app);
    }

    construct {
      var start_action = new SimpleAction ("start", null);
      start_action.activate.connect ((parameter) => {
        this.launcher.join_meeting.begin ((obj, res) => {
          try {
            var app = this.application as MeetingPoint.Application;
            app.connection = this.launcher.join_meeting.end (res);
            app.connection.init.begin ();
            var window = new MainWindow (app);
            this.destroy ();
            window.present ();
          } catch (Error e) {
            print (@"error $(e.message)\n");
          }
        });
      });
      this.add_action (start_action);
    }

    [GtkCallback]
    private void on_username_entry_text_changed () {
      this.launcher.username = this.username_entry.text;
      if (this.username_entry.text.length > 0) {
        this.username_entry.add_css_class ("success");
        this.username_entry.remove_css_class ("error");
        this.username_valid = true;
      } else {
        this.username_entry.add_css_class ("error");
        this.username_entry.remove_css_class ("success");
        this.username_valid = false;
      }
      this.update_button_sensitivity ();
    }

    [GtkCallback]
    private void on_address_entry_text_changed () {
      this.launcher.link = this.address_entry.text.strip ();
      if (this.launcher.link_is_valid) {
        this.launcher.room_exists_already.begin ((obj, res) => {
          bool result = this.launcher.room_exists_already.end (res);
          if (result) {
            this.update_address_entry (true);
          } else {
            this.update_address_entry (false);
          }
        });
      } else {
        this.update_address_entry (false);
      }
    }

    private void update_address_entry (bool valid) {
      this.address_valid = valid;
      if (valid) {
        this.address_entry.add_css_class ("success");
        this.address_entry.remove_css_class ("error");
      } else {
        this.address_entry.add_css_class ("error");
        this.address_entry.remove_css_class ("success");
      }
      this.update_button_sensitivity ();
    }

    private void update_button_sensitivity () {
      if (this.address_valid && this.username_valid) {
        this.start_button.sensitive = true;
      } else {
        this.start_button.sensitive = false;
      }
    }
  }
}
