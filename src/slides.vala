// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  //fixme sealed
  public class SlidesManager : BaseManager {
    public SlidesManager (Connection connection) {
      base (connection, typeof (Slide), "slides", false);
    }

    public Slide? current_slide { get; private set; }

    internal void find_current () {
      for (int i = 0; i < this.items.get_n_items (); i++) {
        Slide slide = (Slide) this.items.get_item (i);
        if (slide.current) {
          this.current_slide = slide;
          return;
        }
      }
    }
  }

  //fixme sealed
  public class Slide : BaseObject, Streamable {
    internal Slide () {}

    public string id { get; private set; }
    // public PresentationPod pod { get; private set; }
    public Presentation presentation { get; private set; }
    public string content { get; private set; }
    public bool current { get; private set; }
    public string image_uri { get; private set; }
    public int64 num { get; private set; }
    public bool safe { get; private set; }
    public string svg_uri { get; private set; }
    public string swf_uri { get; private set; }
    public string thumb_uri { get; private set; }
    public string txt_uri { get; private set; }

    public Gdk.Texture? texture { get; private set; }
    public Gdk.Paintable stream_paintable { get; protected set; }

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      if (fields.has_member ("id")) {
        this.id = fields.get_string_member ("id");
      }
      // if (fields.has_member ("podId")) {
      //   this.pod = this.connection.presentation_pod_manager.get_pod_with_id (fields.get_string_member ("podId"));
      // }
      if (fields.has_member ("presentationId")) {
        this.presentation = this.connection.presentation_manager.get_presentation_with_id (fields.get_string_member ("presentationId"));
      }
      if (fields.has_member ("content")) {
        this.content = fields.get_string_member ("content");
      }
      if (fields.has_member ("current")) {
        this.current = fields.get_boolean_member ("current");
        this.connection.slides_manager.find_current ();
      }
      if (fields.has_member ("imageUri")) {
        this.image_uri = fields.get_string_member ("imageUri");
        this.download.begin ();
      }
      if (fields.has_member ("num")) {
        this.num = fields.get_int_member ("num");
      }
      if (fields.has_member ("safe")) {
        this.safe = fields.get_boolean_member ("safe");
      }
      if (fields.has_member ("svgUri")) {
        this.svg_uri = fields.get_string_member ("svgUri");
      }
      if (fields.has_member ("swfUri")) {
        this.swf_uri = fields.get_string_member ("swfUri");
      }
      if (fields.has_member ("thumbUri")) {
        this.thumb_uri = fields.get_string_member ("thumbUri");
      }
      if (fields.has_member ("txtUri")) {
        this.txt_uri = fields.get_string_member ("txtUri");
      }
      return false;
    }

    private async void download () {
      var message = new Soup.Message ("GET", this.image_uri);
      try {
        Bytes data = yield this.connection.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
        this.stream_paintable = this.texture = Gdk.Texture.from_bytes (data);
      } catch (Error e) {
        return;
      }
    }
  }
}
