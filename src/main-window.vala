// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  [GtkTemplate (ui = "/org/gnome/gitlab/lwildberg/meeting-point/main-window.ui")]
  public class MainWindow : Adw.ApplicationWindow {
    [GtkChild]
    private unowned UserList user_list;
    [GtkChild]
    private unowned ChatView group_chat;
    [GtkChild]
    private unowned Adw.Flap flap;
    [GtkChild]
    private unowned Gtk.ToggleButton flap_toggle;
    [GtkChild]
    private unowned Gtk.DropDown status_dropdown;
    [GtkChild]
    private unowned Gtk.ToggleButton webcam_button;
    [GtkChild]
    private unowned Gtk.ToggleButton listenaudio_button;
    [GtkChild]
    private unowned Gtk.ToggleButton microphone_mute_button;
    [GtkChild]
    private unowned Gtk.ToggleButton speaker_mute_button;
    [GtkChild]
    private unowned Gtk.Box box;
    [GtkChild]
    private unowned Gtk.ToggleButton raise_hand_button;
    [GtkChild]
    internal unowned VideoStreamView video_stream_view;

    public Connection connection { get; private set; }

    public MainWindow (MeetingPoint.Application app) {
      Object (application: app);
    }

    construct {
      this.connection = Application.get_default_connection ();

      this.connection.audio_manager.notify["full-audio"].connect (() => {
        this.microphone_mute_button.active = false;
      });

      if (!this.connection.audio_manager.supports_microphone) {
        var banner = new Adw.Banner ("Microphone Audio is currently unsupported on this server") {
          button_label = "close",
          revealed = true
        };
        banner.button_clicked.connect (() => {
          banner.revealed = false;
        });
        ((Gtk.Box) this.flap.content).prepend (banner);
        this.listenaudio_button.sensitive = false;
      }

      //FIXME
      var factory = new Gtk.SignalListItemFactory ();
      factory.setup.connect (this.status_dropdown_factory_setup);
      factory.bind.connect (this.status_dropdown_factory_bind);
      this.status_dropdown.factory = factory;
    }

    [GtkCallback]
    private void on_webcam_button_toggled () {
      ((Application) this.application).connection.webcam_manager.enabled = this.webcam_button.active;
    }

    [GtkCallback]
    private void on_listenaudio_button_toggled () {
      ((Application) this.application).connection.audio_manager.full_audio = this.listenaudio_button.active;
    }

    [GtkCallback]
    private void on_microphone_mute_button_toggled () {
      ((Application) this.application).connection.audio_manager.microphone_muted = !this.microphone_mute_button.active;
    }

    [GtkCallback]
    private void on_speaker_mute_button_toggled () {
      ((Application) this.application).connection.audio_manager.speaker_muted = !this.speaker_mute_button.active;
    }

    [GtkCallback]
    private void on_status_dropdown_selection_changed () {
      var selected = (Emoji)((Adw.EnumListItem) this.status_dropdown.selected_item).value;
      this.raise_hand_button.active = (selected == Emoji.HAND);
      this.connection.user_manager.current_user.set_emoji_state.begin (selected);
    }

    [GtkCallback]
    private void on_raise_hand_button_toggled () {
      if (this.raise_hand_button.active) {
        this.status_dropdown.selected = 1;
      } else if (this.status_dropdown.selected == 1) {
        this.status_dropdown.selected = 0;
      }
    }

    //[GtkCallback]
    private void status_dropdown_factory_setup (Gtk.ListItem item) {
      assert (item != null);
      assert (item is Gtk.ListItem);
      item.child = new Gtk.Image ();
    }

    //[GtkCallback]
    private void status_dropdown_factory_bind (Gtk.ListItem item) {
      assert (item is Gtk.ListItem);
      ((Gtk.Image) item.child).icon_name = emoji_to_icon_name (((Adw.EnumListItem) item.item).value);
    }
  }

  public string emoji_to_icon_name (MeetingPoint.Emoji emoji) {
    string emoji_name = "";
    switch (emoji) {
      case Emoji.NONE:
        emoji_name = "social-network-none";
        break;
      case Emoji.HAND:
        emoji_name = "hand-open";
        break;
      case Emoji.TIME:
        emoji_name = "clock-alt";
        break;
      case Emoji.UNDECIDED:
        emoji_name = "sentiment-neutral";
        break;
      case Emoji.CONFUSED:
        emoji_name = "dialog-question";
        break;
      case Emoji.SAD:
        emoji_name = "sentiment-dissatisfied";
        break;
      case Emoji.HAPPY:
        emoji_name = "sentiment-satisfied";
        break;
      case Emoji.APPLAUSE:
        emoji_name = "positive-feedback";
        break;
      case Emoji.THUMBS_UP:
        emoji_name = "thumbs-up";
        break;
      case Emoji.THUMBS_DOWN:
        emoji_name = "thumbs-down";
        break;
    }
    return emoji_name;
  }
}

