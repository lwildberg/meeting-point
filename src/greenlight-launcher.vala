// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  private class Greenlight3LauncherImplementation : LauncherImplementation {
    public Greenlight3LauncherImplementation (Launcher launcher) {
      base (launcher, typeof (Greenlight3LauncherImplementation));
    }

    public override async bool start_meeting () throws MeetingError {
      return true;
    }

    private string final_link;

    public override async bool room_exists_already () throws MeetingError {
      var message = new Soup.Message ("GET", this.launcher.link);
      Bytes? data = null;
      bool error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("server error");
      }
      uint length = data.length;
      string content = ((string) Bytes.unref_to_data (data)).substring (0, length);
      string csrf_token = content.split ("csrf-token")[1].split ("\"")[2];
      print ("csrf-token:\n" + csrf_token + "\n");

      Uri? uri = null;
      try {
        uri = Uri.parse (this.launcher.link, UriFlags.NONE);
      } catch (Error e) {
        assert_not_reached ();
      }

      string room_id = uri.get_path ().split ("/")[2];
      string address = Uri.join (UriFlags.NONE, "https", null, uri.get_host (), 443, @"/api/v1/meetings/$room_id/status.json", null, null);
      message = new Soup.Message ("POST", address);
      message.request_headers.append ("X-CSRF-TOKEN", csrf_token);
      message.request_headers.append ("Accept", "application/json");
      var builder = new Json.Builder ();
      builder.begin_object ();
      builder.set_member_name ("name");
      builder.add_string_value (this.launcher.username);
      builder.set_member_name ("access_code");
      builder.add_string_value (this.launcher.password);
      builder.end_object ();
      var generator = new Json.Generator ();
      generator.set_root (builder.get_root ());
      data = new Bytes (generator.to_data (null).data);
      message.set_request_body_from_bytes ("application/json", data);
      // message.add_flags (Soup.MessageFlags.NO_REDIRECT);
      data = null;
      error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("couldn't setup the name");
      }
      length = data.length;
      content = ((string) Bytes.unref_to_data (data)).substring (0, length);
      Json.Object object = Json.from_string (content.replace ("\\\\u0026", "&")).get_object ();
      if (!object.has_member ("data") || !object.get_object_member ("data").get_boolean_member ("status")) {
        return false;
      }
      this.final_link = object.get_object_member ("data").get_string_member ("joinUrl");
      return true;
    }

    public override async Connection join_meeting () throws MeetingError {
      yield this.room_exists_already ();

      var message = new Soup.Message ("GET", this.final_link);
      // message.add_flags (Soup.MessageFlags.NO_REDIRECT);
      Bytes data = null;
      bool error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("server error");
      }

      return yield this.finish_join (message.uri, data);
    }
  }

  private class Greenlight2LauncherImplementation : LauncherImplementation {
    public Greenlight2LauncherImplementation (Launcher launcher) {
      base (launcher, typeof (Greenlight2LauncherImplementation));
    }

    public override async bool start_meeting () throws MeetingError {
      return true;
    }

    private Connection final_connection;

    private string room_name {
      owned get {
        string[] parts = this.launcher.link.split ("/");
        return parts[parts.length-1].dup ();
      }
    }

    private async bool do (bool join) throws MeetingError {
      var message = new Soup.Message ("GET", this.launcher.link);
      Bytes? data = null;
      bool error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("server error");
      }

      if (message.status_code == 404) {
        return false;
      }

      uint length = data.length;
      string content = ((string) Bytes.unref_to_data (data)).substring (0, length);
      string csrf_token = content.split ("csrf-token")[1].split ("\"")[2];
      print ("csrf-token:\n" + csrf_token + "\n");

      Uri? uri = null;
      try {
        uri = Uri.parse (this.launcher.link, UriFlags.NONE);
      } catch (Error e) {
        assert_not_reached ();
      }

      string form = Soup.Form.encode ("utf8", "✓", "authenticity_token", csrf_token);
      string append = @"&%2F$(this.room_name)%5Bsearch%5D=&%2F$(this.room_name)%5Bcolumn%5D=&%2F$(this.room_name)%5Bdirection%5D=&%2F$(this.room_name)%5Bjoin_name%5D=$(this.launcher.username)";
      message = new Soup.Message.from_encoded_form ("POST", this.launcher.link, form + append);
      message.request_headers.append ("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8");
      if (!join) {
        message.add_flags (Soup.MessageFlags.NO_REDIRECT);
      }
      data = null;
      error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("server error");
      }

      if (!join && message.response_headers.get_one ("location") == null) {
        return false;
      }

      if (!join) {
        return true;
      } else {
        this.final_connection = yield this.finish_join (message.uri, data);
        return true; // whatever
      }
    }

    public override async bool room_exists_already () throws MeetingError {
      return yield this.do (false);
    }

    public override async Connection join_meeting () throws MeetingError {
      yield this.do (true);
      return this.final_connection;
    }
  }
}
