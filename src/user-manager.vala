// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME sealed
  public class UserManager : BaseManager {

    public User? current_user { get; internal set; default = null; }

    internal UserManager (Connection connection, string id) {
      base (connection, typeof (User), "users", true);
    }

    internal User? get_user_with_id (string id) {
      for (int i = 0; i < this.items.get_n_items (); i++) {
        User user = (User) this.items.get_item (i);
        if (user.id == id) {
          return user;
        }
      }
      return null;
    }
  }
}
