// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  private class ChatMessageManager : BaseManager {
    public ChatMessageManager (Connection connection) {
      base (connection, typeof (ChatMessage), "group-chat-msg", true);
      this.group_chat_msg_sub = new Array<DdpConnection.Subscription> ();
    }

    private GLib.Array<DdpConnection.Subscription> group_chat_msg_sub;

    public async void subscribe_group_chat_msg (Chat chat) throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_int_value (chat.count);
      builder.end_array ();

      this.group_chat_msg_sub.insert_val (chat.count, yield this.connection.connection.sub ("group-chat-msg", builder.get_root ()));
    }

    internal uint? get_chat_message_with_id (string id) {
      for (uint i = 0; i < this.items.get_n_items (); i++) {
        ChatMessage msg = (ChatMessage) this.items.get_item (i);
        if (msg.id == id) {
          return i;
        }
      }
      return null;
    }
  }

  // FIXME sealed
  public class ChatMessage : BaseObject {

    public DateTime time { get; private set; }
    public string message { get; private set; }
    public User sender { get; internal set; }

    internal Chat chat; // weak ?
    internal string correlation_id; // ?
    internal string id;

    internal ChatMessage () {}

    construct {
      this.sort_func = (a, b) => {
        return ((ChatMessage) a).time.compare (((ChatMessage) b).time);
      };
    }

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      if (fields.has_member ("id"))
        this.id = fields.get_string_member ("id");
      if (fields.has_member ("message"))
        this.message = fields.get_string_member ("message");
      if (fields.has_member ("sender")) {
        this.sender = this.connection.user_manager.get_user_with_id (fields.get_string_member ("sender"));
      }
      if (fields.has_member ("chatId")) {
        this.chat = this.connection.chat_manager.get_chat_with_id (fields.get_string_member ("chatId"));
      }
      if (fields.has_member ("timestamp")) {
        Json.Node n = fields.get_member ("timestamp");
        int64 t = n.get_int ();
        this.time = convert_time (t);
      }
      if (fields.has_member ("correlationId")) {
        this.correlation_id = fields.get_string_member ("correlationId");
      }
      // chat messages usually don't change their timestamp'
      return false;
    }
  }

  // FIXME sealed
  private class ChatMessageBeforeJoinModel : GLib.Object, ListModel {

    private uint n_items;
    private Array<ChatMessage> array;
    private Connection connection;
    private Chat chat;
    private GenericSet<int64?> current_fetches;

    public ChatMessageBeforeJoinModel (Connection connection, Chat chat, uint messages) {
      this.n_items = messages;
      this.connection = connection;
      this.chat = chat;
      this.array = new Array<ChatMessage>.sized (false);
      this.current_fetches = new GenericSet<int64?> (int64_hash, int64_equal);
    }

    private async void fetch_messages_per_page (int64 num) throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.chat.id);
      builder.add_int_value (num);
      builder.end_array ();

      Json.Node result = yield this.connection.connection.method ("fetchMessagePerPage", builder.get_root ());
      assert (result != null);
      Json.Array messages = result.get_array ();
      messages.foreach_element ((a, i, node) => {
        uint pos = (uint) ((num - 1) * 100) + i;
        print ("fetch position: %u\n", pos);
        ChatMessage new_message;
        if (this.array.length <= pos || this.array.data[pos] == null) {
          new_message = new ChatMessage ();
          this.array.insert_val (pos, new_message);
        } else {
          new_message = this.array.data[pos];
        }
        new_message.connection = this.connection;
        new_message.update (node.get_object ());
        if (new_message.sender == null) {
          new_message.sender = new User.already_left (node.get_object ().get_string_member ("senderName"), node.get_object ().get_string_member ("senderRole"));
        }
      });
      this.current_fetches.remove (num);
    }

    public new uint get_n_items () {
      return this.n_items;
    }

    public new Type get_item_type () {
      return typeof (ChatMessage);
    }

    public new Object? get_item (uint position) {
      if (this.array.length > position && this.array.data[position] != null) {
        return this.array.data[position];
      }
      var msg = new ChatMessage ();
      this.array.insert_val (position, msg);
      double tmp;
      tmp = (double) (position + 1) / (double) 100;
      int64 page = ((int64) tmp) + 1;
      if (this.current_fetches.contains (page)) {
        return msg;
      }
      this.current_fetches.add (page);
      this.fetch_messages_per_page.begin (page);
      return msg;
    }
  }
}
