// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  private enum DdpConnectionState {
    CONNECTING,
    CLOSED,
    CLOSING,
    CONNECTED
  }

  // FIXME sealed
  private class DdpConnection : Object {

    public DdpConnection (string host, string path, int port) {
      Object (
        host: host,
        path: path,
        port: port
      );
    }

    construct {
      this.uri = Uri.join (
        GLib.UriFlags.NONE, "wss", null, this.host, this.port, this.path + (this.path.has_suffix ("/") ? "" : "/") + "websocket", null, null
      );
    }

    public string? uri { get; private set; default = null; }
    public string host { get; construct; }
    public string path { get; construct; }
    public int port { get; construct; }

    public DdpConnectionState connection_state { get; private set; default = CLOSED; }

    private string? ddp_session = null;

    private Soup.WebsocketConnection socket;

    public new async void connect (Soup.Session session) throws MeetingError {
      if (this.connection_state != DdpConnectionState.CLOSED) {
        debug ("already connected");
        return;
      }
      debug ("the connection url is %s", this.uri);

      var message = new Soup.Message ("GET", this.uri);
      this.connection_state = DdpConnectionState.CONNECTING;
      bool socket_error = false;
      try {
        this.socket = yield session.websocket_connect_async (message, null, null, Soup.MessagePriority.NORMAL, null);
      } catch (GLib.Error e) {
        socket_error = true;
      }
      if (socket_error) {
        throw new MeetingError.CONNECT ("Error with socket while connecting");
      }
      this.socket.message.connect ((type, message) => {
        debug ("Message arrived:\n" + (string) message.get_data () + "\n");
        Json.Node node;
        try {
          node = Json.from_string ((string) message.get_data ());
        } catch (GLib.Error e) {
          critical (@"Error occurred in DDP socket: $(e.message)\n");
          return;
        }
        Json.Object obj = node.get_object ();
        if (!obj.has_member ("msg")) {
          return;
        }
        string? id = null;
        uint64? intid = null;
        if (obj.has_member ("id")) {
          id = obj.get_string_member ("id");
          intid = uint64.parse (id);
        }
        string msg = obj.get_string_member ("msg");
        switch (msg) {
        case "connected":
          this.ddp_session = obj.get_string_member ("session");
          this.connection_state = DdpConnectionState.CONNECTED;
          Idle.add (connect.callback);
          break;
        case "failed":
          this.connection_state = DdpConnectionState.CLOSED;
          Idle.add (connect.callback);
          break;
        case "ping":
          Json.Builder builder = this.build_msg_begin ("pong");
          if (obj.has_member ("id")) {
            builder.set_member_name ("id");
            builder.add_string_value (id);
          }
          this.build_msg_end_and_send (builder);
          break;
        case "pong":
          if (intid in this.pings) {
            Idle.add ((owned) this.pings[intid].callback);
          }
          break;
        case "nosub":
          if (obj.has_member ("error")) { // should be always true
            this.subs[intid].error = obj.get_member ("error");
          }
          Idle.add ((owned) this.subs[intid].callback);
          break;
        case "added":
          this.on_added (obj.get_string_member ("collection"), id, obj.get_member ("fields").get_object ());
          break;
        case "changed":
          Json.Object fields = obj.has_member ("fields") ? obj.get_object_member ("fields") : new Json.Object ();
          Json.Array cleared = obj.has_member ("cleared") ? obj.get_array_member ("cleared") : new Json.Array ();
          this.on_changed (obj.get_string_member ("collection"), id, fields, cleared);
          break;
        case "removed":
          this.on_removed (obj.get_string_member ("collection"), id);
          break;
        case "ready":
          obj.get_member ("subs").get_array ().foreach_element ((array, index, element) => {
            Idle.add ((owned) this.subs[int64.parse (element.get_string ())].callback);
          });
          break;
        case "addedBefore":
          // not yet implemented
          break;
        case "movedBefore":
          break;
        case "result":
          if (!(intid in this.methods)) { // should always be false
            break;
          }
          if (obj.has_member ("error")) {
            this.methods[intid].error = obj.get_member ("error");
          }
          if (obj.has_member ("result")) {
            this.methods[intid].result = obj.get_member ("result");
          }
          MethodCall call = this.methods[intid];
          if (call.result_or_updated) {
            Idle.add ((owned) call.callback);
          } else {
            call.result_or_updated = true;
            this.methods[intid] = call;
          }
          break;
        case "updated":
          obj.get_array_member ("methods").foreach_element ((array, index, node) => {
            uint64 node_id = int64.parse (node.get_string ());
            MethodCall call = this.methods[node_id];
            if (call.result_or_updated) {
              Idle.add ((owned) call.callback);
            } else {
              call.result_or_updated = true;
              this.methods[node_id] = call;
            }
          });
          break;
        case "error":
          // should be never reached
          critical ("Error in DDP connection\n");
          break;
        default:
          break;
        }
      });
      this.socket.closing.connect(() => {
        debug ("DDP socket closing");
        this.connection_state = DdpConnectionState.CLOSING;
      });
      this.socket.closed.connect (() => {
        debug ("DDP Socket closed");
        this.connection_state = DdpConnectionState.CLOSED;
        Idle.add (connect.callback);
      });
      this.socket.error.connect ((error) => {
        critical (@"DDP Socket error: $(error.message)\n");
      });
      Json.Builder builder = this.build_msg_begin ("connect");
      builder.set_member_name ("version");
      builder.add_string_value ("1");
      builder.set_member_name ("support");
      builder.begin_array ();
      builder.add_string_value ("1");
      builder.end_array ();
      if (this.ddp_session != null) {
        builder.set_member_name ("session");
        builder.add_string_value (this.ddp_session);
      }
      this.build_msg_end_and_send (builder);
      yield;
      if (this.connection_state == DdpConnectionState.CLOSED)
        throw new MeetingError.CONNECT ("Attempt to connect failed with DDP-error");
    }

    private uint64 next_id = 0;

    private struct PingCall {
      public PingCall (owned SourceFunc callback) {
        this.callback = (owned) callback;
      }

      public SourceFunc callback;
    }

    private HashTable<uint64?, PingCall?> pings = new HashTable<uint64?, PingCall?> (int64_hash, int64_equal);

    public async void ping () throws MeetingError {
      if (this.connection_state != DdpConnectionState.CONNECTED) {
        throw new MeetingError.NO_CONNECTION ("For pinging a connection is required.");
      }
      uint64 id = this.next_id;
      this.pings[id] = PingCall (ping.callback);
      this.next_id++;
      Json.Builder builder = this.build_msg_begin ("ping");
      builder.set_member_name ("id");
      builder.add_string_value (id.to_string ());
      this.build_msg_end_and_send (builder);
      yield;
      this.pings.remove (id);
    }

    private struct SubCall {
      public SubCall (owned SourceFunc callback) {
        this.callback = (owned) callback;
      }

      public SourceFunc callback;
      public Json.Node? error;
    }

    private HashTable<uint64?, SubCall?> unsubs = new HashTable<uint64?, SubCall?> (int64_hash, int64_equal);

    // FIXME sealed
    public class Subscription {
      internal Subscription (string name, uint64 id, DdpConnection connection) {
        this.name = name;
        this.id = id;
        this.connection = connection;
      }

      public string name { get; private set; }
      public bool valid { get; private set; default = true; }

      private DdpConnection connection;
      private uint64 id { get; private set; }

      public async void stop () throws MeetingError {
        if (!this.valid) {
          return;
        }
        this.valid = false;
        var sub = SubCall (stop.callback);
        this.connection.unsubs[id] = sub;

        Json.Builder builder = this.connection.build_msg_begin ("unsub");
        builder.set_member_name ("id");
        builder.add_string_value (this.id.to_string ());
        this.connection.build_msg_end_and_send (builder);
        yield;
        if (this.connection.unsubs[id].error != null) {
          this.connection.unsubs.remove (id);
          throw new MeetingError.CONNECTION ("unsubscribing failed");
        }
        this.connection.unsubs.remove (id);
      }
    }

    private HashTable<uint64?, SubCall?> subs = new HashTable<uint64?, SubCall?> (int64_hash, int64_equal);

    public async Subscription sub (string name, Json.Node? parameters = null) throws MeetingError {
      if (this.connection_state != DdpConnectionState.CONNECTED) {
        throw new MeetingError.NO_CONNECTION ("For subscribing a connection is required.");
      }
      uint64 id = this.next_id;
      var sub = SubCall (sub.callback);
      this.next_id++;
      this.subs[id] = sub;
      Json.Builder builder = this.build_msg_begin ("sub");
      builder.set_member_name ("id");
      builder.add_string_value (id.to_string ());
      builder.set_member_name ("name");
      builder.add_string_value (name);
      if (parameters != null) {
        builder.set_member_name ("params");
        builder.add_value (parameters);
      }
      this.build_msg_end_and_send (builder);
      yield;
      if (this.subs[id].error != null) {
        string error = this.subs[id].error.get_object ().get_string_member ("error");
        this.subs.remove (id);
        throw new MeetingError.CONNECTION ("The subscription returned an error: %s".printf (error));
      }
      this.subs.remove (id);
      return new Subscription (name, id, this);
    }

    private struct MethodCall {
      public MethodCall (owned SourceFunc callback) {
        this.callback = (owned) callback;
        this.result_or_updated = false;
      }

      public SourceFunc callback;
      public Json.Node? result;
      public Json.Node? error;
      public bool result_or_updated;
    }

    private HashTable<uint64?, MethodCall?> methods = new HashTable<uint64?, MethodCall?> (int64_hash, int64_equal);

    // TODO return Json.Array
    public async Json.Node? method (string method_name, Json.Node? parameters = null) throws MeetingError {
      if (this.connection_state != DdpConnectionState.CONNECTED) {
        throw new MeetingError.NO_CONNECTION ("For calling a method a connection is required");
      }
      uint64 id = this.next_id;
      var call = MethodCall (method.callback);
      this.next_id++;
      this.methods[id] = call;
      Json.Builder builder = this.build_msg_begin ("method");
      builder.set_member_name ("method");
      builder.add_string_value (method_name);
      if (parameters != null) {
        builder.set_member_name ("params");
        builder.add_value (parameters);
      }
      builder.set_member_name ("id");
      builder.add_string_value (id.to_string ());
      this.build_msg_end_and_send (builder);
      yield;
      if (this.methods[id].error != null)
        throw new MeetingError.CONNECTION ("The method returned an error: %s".printf (this.methods[id].error.get_object ().get_string_member ("error")));
      Json.Node result = this.methods[id].result;
      this.methods.remove (id);
      return result;
    }

    private void send (string message) {
      debug ("Sending DDP message: %s\n", message);
      this.socket.send_text (message);
    }

    public signal void on_added (string collection, string id, Json.Object fields);

    public signal void on_changed (string collection, string id, Json.Object? fields, Json.Array? cleared);

    public signal void on_removed (string collection, string id);

    private Json.Builder build_msg_begin (string msg) {
      var builder = new Json.Builder ();
      builder.begin_object ();
      builder.set_member_name ("msg");
      builder.add_string_value (msg);
      return builder;
    }

    private void build_msg_end_and_send (Json.Builder builder) {
      builder.end_object ();
      var generator = new Json.Generator ();
      Json.Node root = builder.get_root ();
      generator.set_root (root);
      this.send (generator.to_data (null));
    }
  }
}
