// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME sealed
  public class AudioManager : Object {

    public bool full_audio {
      get {
        return this._full_audio;
      } set {
        if (this._full_audio != value) {
          this._full_audio = value;
          this.pipeline.set_state (Gst.State.NULL);
          this.pipeline = new Gst.Pipeline ("pipeline");
          if (value) {
            this.listen_element.disconnect ();
            this.listen_element = new ListenAudioStream (this.connection);
            this.bind_property ("speaker-muted", this.listen_element.volume, "mute", SYNC_CREATE);
            this.pipeline.add (this.full_element);
          } else {
            this.full_element.disconnect ();
            this.full_element = new FullAudioStream (this.connection);
            this.bind_property ("speaker-muted", this.full_element.speaker_mute, "mute", SYNC_CREATE);
            this.bind_property ("microphone-muted", this.full_element.microphone_mute, "mute", SYNC_CREATE);
            this.pipeline.add (this.listen_element);
          }
          if (this.initialized) {
            this.pipeline.set_state (Gst.State.PLAYING);
          }
          // if (value) {
          //   var builder = new Json.Builder ();
          //   builder.begin_array ();
          //   builder.add_null_value ();
          //   builder.add_boolean_value (false);
          //   builder.end_array ();
          //   this.connection.connection.method.begin("toggleVoice", builder.get_root ());
          // }
        }
      }
    }
    public bool speaker_muted { get; set; default = true; }
    public bool microphone_muted { get; set; default = true; }

    internal Connection connection;
    internal Gst.Pipeline pipeline;

    internal ListenAudioStream? listen_element = null;
    internal FullAudioStream? full_element = null;

    private bool initialized = false;
    private bool _full_audio = false;

    internal AudioManager (Connection connection) {
      this.connection = connection;
      this.pipeline = new Gst.Pipeline ("pipeline");
      this.full_element = new FullAudioStream (this.connection);
      this.listen_element = new ListenAudioStream (this.connection);
      this.pipeline.add (this.listen_element);
      this.bind_property ("speaker-muted", this.listen_element.volume, "mute", SYNC_CREATE);
      this.bind_property ("speaker-muted", this.full_element.speaker_mute, "mute", SYNC_CREATE);
      this.bind_property ("microphone-muted", this.full_element.microphone_mute, "mute", SYNC_CREATE);
    }

    internal void init () {
      this.initialized = true;
      this.pipeline.set_state (Gst.State.PLAYING);
    }

    public bool supports_microphone {
      get {
        Json.Object media = this.connection.meteor_settings.get_object_member ("media");
        if (media.has_member ("audio")) {
          Json.Object audio = media.get_object_member ("audio");
          if (audio.has_member ("defaultFullAudioBridge") && audio.get_string_member ("defaultFullAudioBridge") == "fullaudio") {
            return true;
          }
        }
        return false;
      }
    }
  }
}


