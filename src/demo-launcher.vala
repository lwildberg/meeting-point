// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  private class DemoLauncherImplementation : LauncherImplementation {
    public DemoLauncherImplementation (Launcher launcher) {
      base (launcher, typeof (DemoLauncherImplementation));
    }

    public override async bool start_meeting () throws MeetingError {
      return true;
    }

    public override async Connection join_meeting () throws MeetingError {
      Uri? uri = null;
      try {
        uri = Uri.parse (this.launcher.link, UriFlags.NONE);
      } catch (Error e) {
        assert_not_reached ();
      }

      uri = Uri.build (UriFlags.NONE, "https", null, uri.get_host (), 443,
                               "/demo/demoHTML5.jsp", @"username=$(this.launcher.username)&isModerator=true&action=create", null);
      var message = new Soup.Message ("GET", uri.to_string ());
      message.accept_certificate.connect (() => true);
      Bytes? data = null;
      bool error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("server error");
      }

      uint length = data.length;
      string content = ((string) Bytes.unref_to_data (data)).substring (0, length);
      string address = content.split ("window.location.href=")[1].split ("\"")[1];

      message = new Soup.Message ("GET", address.to_string ());
      message.accept_certificate.connect (() => true);
      data = null;
      error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECT ("server error");
      }

      return yield this.finish_join (message.uri, data);
    }

    public override async bool room_exists_already () throws MeetingError {
      return true;
    }
  }
}
