// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  [GtkTemplate (ui = "/org/gnome/gitlab/lwildberg/meeting-point/streamable-view.ui")]
  public class StreamableView : Adw.Bin {
    public bool enable_click { get; set; default = true; }
    public Streamable stream { get; set; }

    private Gtk.GestureClick gesture_click;

    construct {
      this.gesture_click = new Gtk.GestureClick ();
      this.add_controller (this.gesture_click);

      this.gesture_click.pressed.connect (() => {
        if (this.enable_click) {
          ((MainWindow) this.get_root ()).video_stream_view.show_detailed (this.stream);
        }
      });
    }
  }

  [GtkTemplate (ui = "/org/gnome/gitlab/lwildberg/meeting-point/webcam-streamable-view.ui")]
  public class WebcamStreamableView : Adw.Bin {
    [GtkChild]
    private unowned StreamableView streamable_view;
    public bool enable_click { get; set; default = true; }
    public WebcamStreamable stream { get; set; }
  }
}
