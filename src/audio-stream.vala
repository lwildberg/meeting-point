// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME sealed
  private class ListenAudioStream : RecieveStream {

    public Gst.Element volume;

    internal ListenAudioStream (Connection connection) {
      base (connection, StreamType.AUDIO, "recv");

      var queue = Gst.ElementFactory.make ("queue", "queue");
      var convert = Gst.ElementFactory.make ("audioconvert", "convert");
      var resample = Gst.ElementFactory.make ("audioresample", "resample");
      this.volume = Gst.ElementFactory.make ("volume", "volume");
      var sink = Gst.ElementFactory.make ("autoaudiosink", "sink");
      this.add (queue);
      this.add (convert);
      this.add (resample);
      this.add (volume);
      this.add (sink);
      queue.link (convert);
      convert.link (resample);
      resample.link (volume);
      volume.link (sink);

      this.decode_bin.pad_added.connect ((element, pad) => {
        if (!pad.has_current_caps ()) {
          return;
        }
        Gst.Caps? caps = pad.get_current_caps ();
        if (caps == null || caps.get_size () < 1) {
          return;
        }
        unowned Gst.Structure str = caps.get_structure (0);
        print (str.get_name () + "\n");

        pad.link (queue.get_static_pad ("sink"));
        // this.ghost_pad.set_target (resample.get_static_pad ("src")); // TODO
      });
    }

    private static int client_session_number = 1;

    internal async override Json.Builder add_fields_for_start (Json.Builder b) {
      Json.Builder builder = yield base.add_fields_for_start (b);

      add_string_member (builder, "caleeName", "GLOBAL_AUDIO_" + this.connection.meeting_manager.main_meeting.voice_conf);
      builder.set_member_name ("clientSessionNumber"); // ????
      builder.add_int_value (ListenAudioStream.client_session_number);
      ListenAudioStream.client_session_number++;
      // builder.set_member_name ("extension");
      // builder.add_null_value ();

      return builder;
    }

    internal override void state_changed (Gst.State oldstate, Gst.State newstate, Gst.State pending) {
      if (oldstate == Gst.State.PAUSED && newstate == Gst.State.READY) {
        var builder = new Json.Builder ();
        builder.begin_object ();
        add_string_member (builder, "id", "stop");
        add_string_member (builder, "type", this.stream_type.to_string ());
        add_string_member (builder, "role", this.role_string);
        builder.end_object ();
        this.connection.stream_manager.send_message (builder.get_root ());
      }
    }
  }
}
