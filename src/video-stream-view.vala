// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  [GtkTemplate (ui = "/org/gnome/gitlab/lwildberg/meeting-point/video-stream-view.ui")]
  public class VideoStreamView : Adw.Bin, Gtk.Orientable {
    [GtkChild]
    private unowned Gtk.Stack detailed_stack;
    [GtkChild]
    private unowned Gtk.Stack state_stack;
    [GtkChild]
    private unowned Gtk.ListView webcam_presentation_list;
    [GtkChild]
    private unowned Gtk.Box webcam_presentation_box;
    [GtkChild]
    private unowned StreamableView webcam_presentation_streamable_view;
    [GtkChild]
    private unowned StreamableView detailed_view;

    public MeetingPoint.Connection connection { get; private set; }

    private enum LayoutState {
      NOTHING,
      ONLY_ONE_WEBCAM,
      ONLY_WEBCAMS,
      ONLY_PRESENTATION,
      ONLY_SCREENSHARE,
      WEBCAM_PRESENTATION,
      WEBCAM_SCREENSHARE;

      public string to_string () {
        switch (this) {
          case NOTHING:
            return "NOTHING";
          case ONLY_ONE_WEBCAM:
            return "ONLY_ONE_WEBCAM";
          case ONLY_WEBCAMS:
            return "ONLY_WEBCAMS";
          case ONLY_PRESENTATION:
            return "ONLY_PRESENTATION";
          case ONLY_SCREENSHARE:
            return "ONLY_SCREENSHARE";
          case WEBCAM_PRESENTATION:
            return "WEBCAM_PRESENTATION";
          case WEBCAM_SCREENSHARE:
            return "WEBCAM_SCREENSHARE";
          default:
            assert_not_reached ();
        }
      }
    }

    private LayoutState state {
      private set {
        this.state_stack.visible_child_name = value.to_string ();
      }
    }

    public Gtk.Orientation orientation { get; set; default = VERTICAL; }

    internal void show_detailed (Streamable streamable) {
      this.detailed_view.stream = streamable;
      this.detailed_stack.visible_child_name = "DETAILED";
    }

    private void on_layout_changed () {
      uint n_webcams = this.connection.stream_provider.get_n_items ();
      bool presentation = this.connection.presentation_manager.get_n_items () > 0;
      bool screenshare = this.connection.screenshare_manager.get_n_items () > 0;
      if (!presentation && !screenshare) {
        if (n_webcams == 0) {
          this.state = NOTHING;
        } else if (n_webcams == 1) {
          this.state = ONLY_ONE_WEBCAM;
        } else {
          this.state = ONLY_WEBCAMS;
        }
      } else if (screenshare) {
        if (n_webcams > 0) {
          this.state = WEBCAM_SCREENSHARE;
        } else {
          this.state = ONLY_SCREENSHARE;
        }
      } else if (presentation) {
        if (n_webcams > 0) {
          this.state = WEBCAM_PRESENTATION;
        } else {
          this.state = ONLY_PRESENTATION;
        }
      }
    }

    [GtkCallback]
    private void on_detailed_close () {
      this.detailed_view.stream = null;
      this.detailed_stack.visible_child_name = "DEFAULT";
    }

    construct {
      this.connection = Application.get_default_connection ();

      this.connection.stream_provider.items_changed.connect (this.on_layout_changed);
      this.connection.screenshare_manager.items_changed.connect (this.on_layout_changed);
      this.connection.presentation_manager.items_changed.connect (this.on_layout_changed);

      this.notify["orientation"].connect (() => {
        if (this.orientation == HORIZONTAL) {
          this.webcam_presentation_box.orientation = VERTICAL;
          this.webcam_presentation_list.hscroll_policy = NATURAL;
          this.webcam_presentation_list.vscroll_policy = MINIMUM;
          this.webcam_presentation_streamable_view.height_request = 450;
          this.webcam_presentation_streamable_view.width_request = -1;
        } else {
          this.webcam_presentation_box.orientation = HORIZONTAL;
          this.webcam_presentation_list.hscroll_policy = MINIMUM;
          this.webcam_presentation_list.vscroll_policy = NATURAL;
          this.webcam_presentation_streamable_view.height_request = -1;
          this.webcam_presentation_streamable_view.width_request = 300;
        }
      });
    }
  }
}
