// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

using Gtk 4.0;
using Adw 1;

template $MeetingPointMainWindow : Adw.ApplicationWindow {
  default-width: 800;
  default-height: 600;
  title: "Meeting Point";

  styles ["devel"]

  Box {
    orientation: vertical;

    HeaderBar header_bar {
      [start]
      ToggleButton flap_toggle {
        icon-name: "dock-left-symbolic";
        active: true;
      }

      [end]
      MenuButton {
        icon-name: "open-menu-symbolic";
        menu-model: primary_menu;
      }
    }

    Adw.Flap flap {
      reveal-flap: bind-property flap_toggle.active bidirectional;

      [content]
      Box box {
        orientation: vertical;

        $MeetingPointVideoStreamView video_stream_view {
          orientation: horizontal;
        }

        Separator {}

        CenterBox {
          orientation: horizontal;

          [start]
          Box {
            DropDown status_dropdown {
              model: Adw.EnumListModel {
                enum-type: typeof<$MeetingPointEmoji>;
              };
              factory: SignalListItemFactory {
                //setup => $status_dropdown_factory_setup();
                //bind => $status_dropdown_factory_bind();
              };
              notify::selected => $on_status_dropdown_selection_changed();
              valign: center;
              styles ["circular", "suggested-action"]
            }
          }

          [center]
          Box {
            spacing: 6;

            ToggleButton webcam_button {
              toggled => $on_webcam_button_toggled();
              icon-name: "camera-video";
              valign: center;
              styles ["circular", "suggested-action"]
            }

            ToggleButton listenaudio_button {
              toggled => $on_listenaudio_button_toggled();
              child: Label {
                label: "View Only/Share";
                ellipsize: middle;
              };
              valign: center;
            }

            ToggleButton microphone_mute_button {
              toggled => $on_microphone_mute_button_toggled();
              sensitive: bind listenaudio_button.active;
              icon-name: "audio-input-microphone";
              valign: center;
              styles ["circular", "suggested-action"]
            }

            ToggleButton speaker_mute_button {
              toggled => $on_speaker_mute_button_toggled();
              icon-name: "audio-headphones-symbolic";
              valign: center;
              styles ["circular", "suggested-action"]
            }
          }

          [end]
          Box {
            ToggleButton raise_hand_button {
              icon-name: "hand-open";
              valign: center;
              toggled => $on_raise_hand_button_toggled();
              styles ["circular", "suggested-action"]
            }
          }

          styles ["toolbar"]
        }
      }

      [flap]
      Box {
        orientation: horizontal;

        styles ["background"]

        $MeetingPointUserList user_list {
          width-request: 170;
          hexpand-set: true;
          hexpand: false;
        }

        Separator {}

        $MeetingPointChatView group_chat {
          width-request: 170;
          hexpand-set: true;
          hexpand: false;
          chat: bind template.application as <$MeetingPointApplication>.connection as <$MeetingPointConnection>.chat_manager as <$MeetingPointChatManager>.room_chat;
        }
      }

      [separator]
      Separator {}
    }
  }
}

menu primary_menu {
  section {
    item {
      label: _("_Preferences");
      action: "app.preferences";
    }

    item {
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("_About Meeting Point");
      action: "app.about";
    }
  }
}