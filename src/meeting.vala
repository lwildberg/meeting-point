// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME sealed
  public class MeetingManager : BaseManager {

    internal MeetingManager (Connection connection) {
      base (connection, typeof (Meeting), "meetings", false);
    }

    private DdpConnection.Subscription meetings_sub;

    public weak Meeting main_meeting { get; internal set; }
  }

  // FIXME sealed
  public class Meeting : BaseObject {

    public string meeting_id { get; private set; }
    public bool breakout_free_join { get; private set; }
    public string breakout_parent_id { get; private set; }
    public bool breakout_private_chat_enabled { get; private set; }
    public bool breakout_record { get; private set; }
    public int64 breakout_sequence { get; private set; }
    public int64 created_time { get; private set; }
    public int64 duration { get; private set; }
    public bool end_when_no_moderator { get; private set; }
    public int64 end_when_no_moderator_delay_in_minutes { get; private set; }
    public int64 meeting_expire_if_no_user_joined_in_minutes { get; private set; }
    public int64 meeting_expire_when_last_user_left_in_minutes { get; private set; }
    public int64 time_remaining { get; private set; }
    public int64 user_activity_sign_response_delay_in_minutes { get; private set; }
    public int64 user_inactivity_inspect_timer_in_minutes { get; private set; }
    public int64 user_inactivity_treshold_in_minutes { get; private set; }
    public string guest_lobby_message { get; private set; }
    public string layout { get; private set; }
    public bool disable_cam { get; private set; }
    public bool disable_mic { get; private set; }
    public bool disable_notes { get; private set; }
    public bool disable_private_chat { get; private set; }
    public bool disable_public_chat { get; private set; }
    public bool hide_user_list { get; private set; }
    public bool hide_viewers_cursor { get; private set; }
    public bool lock_on_join { get; private set; }
    public bool lock_on_join_configurable { get; private set; }
    public bool locked_layout { get; private set; }
    public string set_by { get; private set; }
    public bool meeting_ended { get; private set; }
    public string ext_id { get; private set; }
    public string int_id { get; private set; }
    public bool is_breakout { get; private set; }
    public int64 meeting_camera_cap { get; private set; }
    public string name { get; private set; }
    public bool published_poll { get; private set; }
    public int64 html5_instance_id { get; private set; }
    public bool allow_mods_to_eject_cameras { get; private set; }
    public bool allow_mods_to_unmute_users { get; private set; }
    public bool authenticated_guest { get; private set; }
    public string guest_policy { get; private set; }
    public int64 max_users { get; private set; }
    public string meeting_layout { get; private set; }
    public int64 user_camera_cap { get; private set; }
    public bool webcams_only_for_moderator { get; private set; }
    public string dial_number { get; private set; }
    public bool mute_on_start { get; private set; }
    public string tel_voice { get; private set; }
    public string voice_conf { get; private set; }
    public string welcome_message { get; private set; }

    internal Meeting () {}

    internal override bool update (Json.Object fields, Json.Array? cleared = null) { // TODO
      if (fields.has_member ("meetingId")) {
        this.meeting_id = fields.get_string_member ("meetingId");
      }
      if (fields.has_member ("breakoutProps")) {
        Json.Object o = fields.get_object_member ("breakoutProps");
        if (o.has_member ("freeJoin")) {
          this.breakout_free_join = o.get_boolean_member ("freeJoin");
        }
        if (o.has_member ("parentId")) {
          this.breakout_parent_id = o.get_string_member ("parentId");
          if (this.breakout_parent_id == "bbb-none") {
            this.connection.meeting_manager.main_meeting = this;
          }
        }
        if (o.has_member ("privateChatEnabled")) {
          this.breakout_private_chat_enabled = o.get_boolean_member ("privateChatEnabled");
        }
        if (o.has_member ("record")) {
          this.breakout_record = o.get_boolean_member ("record");
        }
        if (o.has_member ("sequence")) {
          this.breakout_sequence = o.get_int_member ("sequence");
        }
      }
      if (fields.has_member ("durationProps")) {
        Json.Object o = fields.get_object_member ("durationProps");
        if (o.has_member ("createdTime")) {
          this.created_time = o.get_int_member ("createdTime");
        }
        if (o.has_member ("duration")) {
          this.duration = o.get_int_member ("duration");
        }
        if (o.has_member ("endWhenNoModerator")) {
          this.end_when_no_moderator = o.get_boolean_member ("endWhenNoModerator");
        }
        if (o.has_member ("endWhenNoModeratorDelayInMinutes")) {
          this.end_when_no_moderator_delay_in_minutes = o.get_int_member ("endWhenNoModeratorDelayInMinutes");
        }
        if (o.has_member ("meetingExpireIfNoUserJoinedInMinutes")) {
          this.meeting_expire_if_no_user_joined_in_minutes = o.get_int_member ("meetingExpireIfNoUserJoinedInMinutes");
        }
        if (o.has_member ("meetingExpireWhenLastUserLeftInMinutes")) {
          this.meeting_expire_when_last_user_left_in_minutes = o.get_int_member ("meetingExpireWhenLastUserLeftInMinutes");
        }
        if (o.has_member ("timeRemaining")) {
          this.time_remaining = o.get_int_member ("timeRemaining");
        }
        if (o.has_member ("userActivitySignResponseDelayInMinutes")) {
          this.user_activity_sign_response_delay_in_minutes = o.get_int_member ("userActivitySignResponseDelayInMinutes");
        }
        if (o.has_member ("userInactivityInspectTimerInMinutes")) {
          this.user_inactivity_inspect_timer_in_minutes = o.get_int_member ("userInactivityInspectTimerInMinutes");
        }
        if (o.has_member ("userInactivityThresholdInMinutes")) {
          this.user_inactivity_treshold_in_minutes = o.get_int_member ("userInactivityThresholdInMinutes");
        }
      }
      if (fields.has_member ("guestLobbyMessage")) {
        this.guest_lobby_message = fields.get_string_member ("guestLobbyMessage");
      }
      if (fields.has_member ("layout")) {
        this.layout = fields.get_string_member ("layout");
      }
      if (fields.has_member ("lockSettingsProps")) {
        Json.Object o = fields.get_object_member ("lockSettingsProps");
        if (o.has_member ("disableCam")) {
          this.disable_cam = o.get_boolean_member ("disableCam");
        }
        if (o.has_member ("disableMic")) {
          this.disable_mic = o.get_boolean_member ("disableMic");
        }
        if (o.has_member ("disableNotes")) {
          this.disable_notes = o.get_boolean_member ("disableNotes");
        }
        if (o.has_member ("disablePrivateChat")) {
          this.disable_private_chat = o.get_boolean_member ("disablePrivateChat");
        }
        if (o.has_member ("disablePublicChat")) {
          this.disable_public_chat = o.get_boolean_member ("disablePublicChat");
        }
        if (o.has_member ("hideUserList")) {
          this.hide_user_list = o.get_boolean_member ("hideUserList");
        }
        if (o.has_member ("hideViewersCursor")) {
          this.hide_viewers_cursor = o.get_boolean_member ("hideViewersCursor");
        }
        if (o.has_member ("lockOnJoin")) {
          this.lock_on_join = o.get_boolean_member ("lockOnJoin");
        }
        if (o.has_member ("lockOnJoinConfigurable")) {
          this.lock_on_join_configurable = o.get_boolean_member ("lockOnJoinConfigurable");
        }
        if (o.has_member ("lockedLayout")) {
          this.locked_layout = o.get_boolean_member ("lockedLayout");
        }
        if (o.has_member ("setBy")) {
          this.set_by = o.get_string_member ("setBy");
        }
      }
      if (fields.has_member ("meetingEnded")) {
        this.meeting_ended = fields.get_boolean_member ("meetingEnded");
      }
      if (fields.has_member ("meetingProp")) {
        Json.Object o = fields.get_object_member ("meetingProp");
        // disabled features TODO
        if (o.has_member ("extId")) {
          this.ext_id = o.get_string_member ("extId");
        }
        if (o.has_member ("intId")) {
          this.int_id = o.get_string_member ("intId");
        }
        if (o.has_member ("isBreakout")) {
          this.is_breakout = o.get_boolean_member ("isBreakout");
        }
        if (o.has_member ("meetingCameraCap")) {
          this.meeting_camera_cap = o.get_int_member ("meetingCameraCap");
        }
        if (o.has_member ("name")) {
          this.name = o.get_string_member ("name");
        }
      }
      // metadata prop ?
      if (fields.has_member ("publishedPoll")) {
        this.published_poll = fields.get_boolean_member ("publishedPoll");
      }
      // randomly selected user
      if (fields.has_member ("systemProps")) {
        Json.Object o = fields.get_object_member ("systemProps");
        if (o.has_member ("html5InstanceId")) {
          this.html5_instance_id = o.get_int_member ("html5InstanceId"); // ?
        }
      }
      if (fields.has_member ("userProps")) {
        Json.Object o = fields.get_object_member ("userProps");
        if (o.has_member ("allowModsToEjectCameras")) {
          this.allow_mods_to_eject_cameras = o.get_boolean_member ("allowModsToEjectCameras");
        }
        if (o.has_member ("allowModsToUnmuteUsers")) {
          this.allow_mods_to_unmute_users = o.get_boolean_member ("allowModsToUnmuteUsers");
        }
        if (o.has_member ("authenticatedGuest")) {
          this.authenticated_guest = o.get_boolean_member ("authenticatedGuest");
        }
        if (o.has_member ("guestPolicy")) {
          this.guest_policy = o.get_string_member ("guestPolicy");
        }
        if (o.has_member ("maxUsers")) {
          this.max_users = o.get_int_member ("maxUsers");
        }
        if (o.has_member ("meetingLayout")) {
          this.meeting_layout = o.get_string_member ("meetingLayout");
        }
        if (o.has_member ("userCameraCap")) {
          this.user_camera_cap = o.get_int_member ("userCameraCap");
        }
        if (o.has_member ("webcamsOnlyForModerator")) {
          this.webcams_only_for_moderator = o.get_boolean_member ("webcamsOnlyForModerator");
        }
      }
      if (fields.has_member ("voiceProp")) {
        Json.Object o = fields.get_object_member ("voiceProp");
        if (o.has_member ("dialNumber")) {
          this.dial_number = o.get_string_member ("dialNumber");
        }
        if (o.has_member ("muteOnStart")) {
          this.mute_on_start = o.get_boolean_member ("muteOnStart");
        }
        if (o.has_member ("telVoice")) {
          this.tel_voice = o.get_string_member ("telVoice");
        }
        if (o.has_member ("voiceConf")) {
          this.voice_conf = o.get_string_member ("voiceConf");
        }
      }
      if (fields.has_member ("welcomeProp")) {
        Json.Object o = fields.get_object_member ("welcomeProp");
        if (o.has_member ("welcomeMsg")) {
          this.welcome_message = o.get_string_member ("welcomeMsg");
        }
        // welcomeMsgTemplate ??
      }
      return false;
    }
  }
}
