// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {

  public enum AccessType {
    PUBLIC,
    PRIVATE;

    internal static AccessType from_string (string access) {
      switch (access) {
        case "PUBLIC_ACCESS":
          return PUBLIC;
        case "PRIVATE_ACCESS":
          return PRIVATE;
      }
      assert_not_reached ();
    }
  }

  public class Chat : BaseObject, ListModel {

    public string id { get; internal set; }
    public AccessType access { get; private set; }
    public weak User? created_by { get; private set; }
    public string name { get; private set; }
    public ListModel users { get; private set; default = new ListStore (typeof (User)); }

    internal uint messages_before_join;
    internal uint count;

    private static uint next_count = 0;
    private ChatMessageBeforeJoinModel messages_before_join_model;
    public ListModel flatten_model { get; private set; }

    public ListModel users_typing { get; internal set; }

    internal Chat () {}

    construct {
      this.count = Chat.next_count;
      Chat.next_count++;

      this.sort_func = (a, b) => {
        return strcmp (((Chat) a).name, ((Chat) b).name);
      };
    }

    internal override void init () {
      var filter = new Gtk.CustomFilter ((item) => {
        var user_typing = item as UserTyping;
        return user_typing.chat == this;
      });
      var users_typing_filter_model = new Gtk.FilterListModel (this.connection.users_typing_manager, filter);
      this.users_typing = new Gtk.MapListModel (users_typing_filter_model, (item) => {
        var user_typing = item as UserTyping;
        return user_typing.user;
      });

      this.flatten_model = new Gtk.FlattenListModel (new ListStore (typeof (ListModel)));

      this.flatten_model.items_changed.connect ((position, removed, added) => {
        this.items_changed (position, removed, added);
      });

      filter = new Gtk.CustomFilter ((item) => {
        var message = item as ChatMessage;
        return message.chat == this;
      });
      var messages_filter_model = new Gtk.FilterListModel (this.connection.chat_message_manager, filter);

      ((ListStore) ((Gtk.FlattenListModel) this.flatten_model).model).append (messages_filter_model);
    }

    internal void set_messages_before_join (uint messages) {
      this.messages_before_join = messages;
      this.messages_before_join_model = new ChatMessageBeforeJoinModel (this.connection, this, messages);
      var first = ((Gtk.FlattenListModel) this.flatten_model).model.get_item (0);
      ((ListStore) ((Gtk.FlattenListModel) this.flatten_model).model).remove_all ();
      ((ListStore) ((Gtk.FlattenListModel) this.flatten_model).model).append (this.messages_before_join_model);
      ((ListStore) ((Gtk.FlattenListModel) this.flatten_model).model).append (first);
    }

    internal override bool update (Json.Object fields, Json.Array? cleared = null) {
      bool changed = false;
      if (fields.has_member ("chatId"))  {// when does that happen ?
        this.id = fields.get_string_member ("chatId");
        if (this.id == "MAIN-PUBLIC-GROUP-CHAT") {
           this.connection.chat_manager.room_chat = this;
        }
      }
      if (fields.has_member ("access")) {
        this.access = AccessType.from_string (fields.get_string_member ("access"));
      }
      if (fields.has_member ("name")) {
        this.name = fields.get_string_member ("name");
        changed = true;
      }

      // "participants" are not needed, because "users" has all information already

      if (fields.has_member ("users")) {
        Json.Array? user_array = fields.get_array_member ("users");
        ((ListStore) this.users).remove_all ();
        user_array.foreach_element ((element, index, node) => {
          User user = this.connection.user_manager.get_user_with_id (node.get_string ());
          ((ListStore) this.users).insert_sorted (user, (a, b) => {
            return strcmp (((User) a).name, ((User) b).name);
          });
        });
      }

      if (fields.has_member ("createdBy")) {
        string creator = fields.get_string_member ("createdBy");
        if (creator == "SYSTEM") {
          this.created_by = null;
        } else {
          User? user = this.connection.user_manager.get_user_with_id (creator);
          this.created_by = user;
        }
      }

      User? other = this.get_other_user ();
      if (other != null) {
        other.chat = this;
      }

      this.sub_chat.begin ();

      return changed;
    }

    private async void sub_chat () throws MeetingError {
      yield this.connection.chat_message_manager.subscribe_group_chat_msg (this);
    }

    // FIXME make property
    public User? get_other_user () {
      if (this.users.get_n_items () != 2 && this.access != PRIVATE) {
        return null;
      }
      for (uint i = 0; i < this.users.get_n_items (); i++) {
        User user = (User) this.users.get_item (i);
        if (!user.equals (this.connection.user_manager.current_user)) {
          return user;
        }
      }
      return null;
    }

    public async void send_message (string message) throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.id);
      builder.begin_object ();
      builder.set_member_name ("correlationId");
      builder.add_string_value (@"$(this.connection.user_manager.current_user.id)-$(new DateTime.now ().to_unix () * 1000)");
      builder.set_member_name ("sender");
      builder.begin_object ();
      builder.set_member_name ("id");
      builder.add_string_value (this.connection.user_manager.current_user.id);
      builder.set_member_name ("name");
      builder.add_string_value (this.connection.user_manager.current_user.name);
      builder.set_member_name("role");
      // builder.add_string_value (this.connection.user_manager.current_user.role.to_string ());
      builder.add_string_value ("");
      builder.end_object ();
      builder.set_member_name ("chatEmphasizedText");
      builder.add_boolean_value (true);
      builder.set_member_name ("message");
      builder.add_string_value (message);
      builder.end_object ();
      builder.end_array ();

      yield this.connection.connection.method ("sendGroupChatMsg", builder.get_root ());
    }

    public async void start_typing () throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.id == "MAIN-PUBLIC-GROUP-CHAT" ? "public" : this.id);
      builder.end_array ();

      yield this.connection.connection.method ("startUserTyping", builder.get_root ());
    }

    public async void end_typing () throws MeetingError {
      var builder = new Json.Builder ();
      builder.begin_array ();
      builder.add_string_value (this.id == "MAIN-PUBLIC-GROUP-CHAT" ? "public" : this.id);
      builder.end_array ();

      yield this.connection.connection.method ("endUserTyping", builder.get_root ());
    }

    public new Type get_item_type () {
      return typeof (ChatMessage);
    }

    public new uint get_n_items () {
      return this.flatten_model.get_n_items ();
    }

    public new Object? get_item (uint position) {
      return this.flatten_model.get_item (position);
    }
  }
}
