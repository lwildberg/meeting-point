// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {

  public enum Emoji {
    NONE,
    HAND,
    TIME,
    UNDECIDED,
    CONFUSED,
    SAD,
    HAPPY,
    APPLAUSE,
    THUMBS_UP,
    THUMBS_DOWN;

    internal unowned string to_string () {
      switch (this) {
        case HAND:
          return "hand";
        case TIME:
          return "time";
        case UNDECIDED:
          return "undecided";
        case CONFUSED:
          return "confused";
        case SAD:
          return "sad";
        case HAPPY:
          return "happy";
        case APPLAUSE:
          return "applause";
        case THUMBS_UP:
          return "thumbs_up";
        case THUMBS_DOWN:
          return "thumbs_down";
        // case NONE:
        default:
          return "none";
      }
    }

    internal static Emoji from_string (string emoji) {
      switch (emoji) {
        case "hand":
          return HAND;
        case "time":
          return TIME;
        case "undecided":
          return UNDECIDED;
        case "confused":
          return CONFUSED;
        case "sad":
          return SAD;
        case "happy":
          return HAPPY;
        case "applause":
          return APPLAUSE;
        case "thumbs_up":
          return THUMBS_UP;
        case "thumbs_down":
          return THUMBS_DOWN;
        // case "none":
        default:
          return NONE;
      }
    }
  }
}
