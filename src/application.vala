// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  public class Application : Adw.Application {
    public Application () {
      Object (
        application_id: "app.drey.MeetingPoint.Devel.MeetingPoint.Devel",
        flags: ApplicationFlags.FLAGS_NONE,
        resource_base_path: "/org/gnome/gitlab/lwildberg/meeting-point"
      );
    }

    public MeetingPoint.Connection connection { get; set; }

    public static Connection get_default_connection () {
      return ((MeetingPoint.Application) GLib.Application.get_default ()).connection;
    }

    construct {
      ActionEntry[] action_entries = {
        { "about", this.on_about_action },
        { "preferences", this.on_preferences_action },
        { "quit", this.quit }
      };
      this.add_action_entries (action_entries, this);
      this.set_accels_for_action ("app.quit", {"<primary>q"});
    }

    public override void activate () {
      base.activate ();
      var win = this.active_window;
      if (win == null) {
        win = new LauncherWindow (this);
      }
      win.present ();
    }

    private void on_about_action () {
      string[] developers = { "Lorenz Wildberg" };
      string[] designers = { "Tobias Bernard" };
      var about = new Adw.AboutWindow () {
        transient_for = this.active_window,
        application_name = "Meeting Point",
        application_icon = "app.drey.MeetingPoint",
        developer_name = "Lorenz Wildberg",
        version = "0.2.0",
        developers = developers,
        designers = designers,
        copyright = "© 2022 Lorenz Wildberg",
        license_type = Gtk.License.GPL_3_0
      };

      about.present ();
    }

    private void on_preferences_action () {
      message ("app.preferences action activated");
    }
  }

  int main (string[] args) {
    Gst.init (ref args);

    var app = new Application ();
    return app.run (args);
  }
}
