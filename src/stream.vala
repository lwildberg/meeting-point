// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME private
  public abstract class Stream : Gst.Bin {

    // protected
    internal weak Connection connection;
    internal Gst.Element webrtc_bin;
    internal StreamType stream_type;
    internal string role_string;

    internal Stream (Connection connection, StreamType stream_type, string role_string, Gst.Element webrtc_bin, string? camera_id = null) {
      this.connection = connection;
      this.stream_type = stream_type;
      this.role_string = role_string;
      this.webrtc_bin = webrtc_bin;

      Signal.connect (this.webrtc_bin, "on-negotiation-needed", (Callback) Stream.on_negotiation_needed, this);

      // FIXME bbb
      // Signal.connect (this.webrtc_bin, "on-ice-candidate", (Callback) Stream.on_ice_candidate_for_sending, this);

      this.webrtc_bin.notify["connection-state"].connect (() => {
        Gst.WebRTCPeerConnectionState state;
        this.webrtc_bin.get ("connection-state", out state);
        debug (@"webrtc connection state changed: $state");
      });
    }

    internal new virtual void disconnect () {
      // FIXME should be done in destructor, but it doesn't get called (because of memory leak probably)
      this.connection.stream_manager.on_start_response.disconnect (this.on_start_response);
      this.connection.stream_manager.on_ice_candidate.disconnect (this.on_ice_candidate_for_adding);
      this.connection.stream_manager.on_play_start.disconnect (this.on_play_start);
      this.connection.stream_manager.on_play_stop.disconnect (this.on_play_stop);
    }

    internal new virtual void connect () {
      string detail = this.stream_type.to_string ();
      if (this is VideoShareStream) {
        detail += "-" + ((VideoShareStream) this).camera_id;
      } else if (this is ViewerVideoStream) {
        detail += "-" + ((ViewerVideoStream) this).camera_id;
      }
      this.connection.stream_manager.on_start_response[detail].connect (this.on_start_response);
      this.connection.stream_manager.on_ice_candidate[detail].connect (this.on_ice_candidate_for_adding);
      this.connection.stream_manager.on_play_start[detail].connect (this.on_play_start);
      this.connection.stream_manager.on_play_stop[detail].connect (this.on_play_stop);
    }

    private static void on_negotiation_needed (Gst.Element element, Stream this_stream) {
      print ("on-negotiation-needed\n");
      assert (this_stream is Stream);
      this_stream.connect ();

      var builder = new Json.Builder ();
      builder.begin_object ();

      this_stream.add_fields_for_start.begin (builder, (obj, res) => {
        builder = this_stream.add_fields_for_start.end (res);
        builder.end_object ();
        this_stream.connection.stream_manager.send_message (builder.get_root ());
      });
    }

    // chain up first
    internal virtual async Json.Builder add_fields_for_start (Json.Builder builder) {
      add_string_member (builder, "id", "start");
      add_string_member (builder, "type", this.stream_type.to_string ());
      add_string_member (builder, "role", this.role_string);
      add_string_member (builder, "meetingId", this.connection.meeting_id);
      add_string_member (builder, "voiceBridge", this.connection.meeting_manager.main_meeting.voice_conf);
      add_string_member (builder, "userId", this.connection.user_manager.current_user.id);
      add_string_member (builder, "userName", this.connection.user_manager.current_user.name);
      Json.Object kurento = this.connection.meteor_settings.get_object_member ("kurento");
      if (kurento.has_member ("mediaserver")) {
        add_string_member (builder, "mediaserver", kurento.get_string_member ("videoMediaServer"));
      }
      return builder;
    }

    internal void on_start_response (Json.Object object) {
      string sdp_answer = object.get_string_member ("sdpAnswer");
      print ("on-start-response\n");
      Gst.SDP.Message? message = null;
      Gst.SDP.Message.new_from_text (sdp_answer, out message);
      var description = new Gst.WebRTCSessionDescription ((this is RecieveStream) ? Gst.WebRTCSDPType.OFFER : Gst.WebRTCSDPType.ANSWER, message);
      var promise = new Gst.Promise.with_change_func ((promise) => {
        promise.wait ();
        print ("on-remote-description-set\n");
        if (this is RecieveStream) {
          ((RecieveStream) this).send_subscriber_answer.begin ();
        }
      });
      Signal.emit_by_name (this.webrtc_bin, "set-remote-description", description, promise);
    }

    private static void on_ice_candidate_for_sending (Gst.Element element, int64 sdp_mline_index, string candidate, Stream stream) {
      print ("on-ice-candidate-for-sending\n");
      assert (stream is Stream);
      var builder = new Json.Builder ();
      builder.begin_object ();
      add_string_member (builder, "id", "iceCandidate");
      add_string_member (builder, "role", stream.role_string); // TODO
      add_string_member (builder, "type", stream.stream_type.to_string ());
      add_string_member (builder, "candidate", candidate);
      builder.end_object ();
      stream.connection.stream_manager.send_message (builder.get_root ());
    }

    private void on_ice_candidate_for_adding (Json.Object object) {
      print ("on-ice-candidate-for-adding\n");
      int64 sdp_mline_index = object.get_object_member ("candidate").get_int_member ("sdpMLineIndex");
      string candidate = object.get_object_member ("candidate").get_string_member ("candidate");
      Signal.emit_by_name (this.webrtc_bin, "add-ice-candidate", sdp_mline_index, candidate);
    }

    internal virtual void on_play_start () {
      //
    }

    private void on_play_stop () {
      //
    }

    internal void set_local_description (Gst.WebRTCSessionDescription description) {
      var promise = new Gst.Promise.with_change_func ((promise) => {
        promise.wait ();
        print ("on-local-description-set\n");
      });
      Signal.emit_by_name (this.webrtc_bin, "set-local-description", description, promise);
      promise.interrupt ();
    }

    internal void set_remote_description (Gst.WebRTCSessionDescription description) {
      var promise = new Gst.Promise.with_change_func ((promise) => {
        promise.wait ();
        print ("on-remote-description-set\n");
      });
      Signal.emit_by_name (this.webrtc_bin, "set-remote-description", description, promise);
      promise.interrupt ();
    }

    // true => offer, false => answer
    protected async string create_answer_or_offer (bool offer) {
      SourceFunc func = create_answer_or_offer.callback;
      string name = "offer";
      if (!offer) {
        name = "answer";
      }
      var promise = new Gst.Promise.with_change_func ((promise) => {
        promise.wait ();
        print ("on-%s-created\n", name);
        Idle.add (func);
      });
      Signal.emit_by_name (this.webrtc_bin, "create-" + name, null, promise);
      yield;
      unowned Gst.Structure reply = promise.get_reply ();
      Value answer_value = reply.get_value (name);
      var dsc = (Gst.WebRTCSessionDescription) answer_value;

      this.set_local_description (dsc);

      return dsc.sdp.as_text ();
    }
  }
}
