// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME private
  public abstract class RecieveStream : Stream {
    internal RecieveStream (Connection connection, StreamType stream_type, string role_string, string? camera_id = null) {
      var webrtc_bin = Gst.ElementFactory.make ("webrtcbin", "webrtc_bin");
      assert (webrtc_bin != null);
      base (connection, stream_type, role_string, webrtc_bin, camera_id);

      this.add (webrtc_bin);
      this.decode_bin = Gst.ElementFactory.make ("decodebin", "decode_bin");
      this.add (this.decode_bin);

      this.webrtc_bin.pad_added.connect ((element, pad) => {
        print ("pad-added\n");
        if (pad.direction != Gst.PadDirection.SRC) {
          return;
        }
        this.webrtc_bin.link (this.decode_bin);
      });
    }

    internal Gst.Element decode_bin;

    internal async void send_subscriber_answer () {
      var builder = new Json.Builder ();
      builder.begin_object ();

      this.add_fields_for_subscriber_answer (ref builder);

      add_string_member (builder, (this.stream_type == AUDIO) ? "sdpOffer" : "answer", yield this.create_answer_or_offer (false));
      builder.end_object ();
      this.connection.stream_manager.send_message (builder.get_root ());
      print ("on-local-description-sended\n");
    }

    // chainup first
    internal virtual void add_fields_for_subscriber_answer (ref Json.Builder builder) {
      add_string_member (builder, "id", "subscriberAnswer");
      add_string_member (builder, "type", this.stream_type.to_string ());
      add_string_member (builder, "role", this.role_string);
      add_string_member (builder, "voiceBridge", this.connection.meeting_manager.main_meeting.voice_conf);
    }

    internal override async Json.Builder add_fields_for_start (Json.Builder builder) {
      return yield base.add_fields_for_start (builder);
    }
  }
}
