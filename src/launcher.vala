// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME sealed
  public class Launcher : Object {
    construct {
      this.session = new Soup.Session ();
      this.session.add_feature (new Soup.Logger (BODY));

      var cookie = new Soup.Cookie ("cookie_consented_a", "true", ".senfcall.de", "/", Soup.COOKIE_MAX_AGE_ONE_WEEK);
      var cookie_jar = new Soup.CookieJar ();
      cookie_jar.add_cookie (cookie);
      this.session.add_feature (cookie_jar);

      this.implementation = new SenfcallLauncherImplementation (this);

      this.notify["link"].connect (() => {
        if (Regex.match_simple ("https://public.senfcall.de/.+", this.link)) {
          if (this.implementation.type != typeof (SenfcallLauncherImplementation)) {
            this.implementation = new SenfcallLauncherImplementation (this);
          }
        } else if (Regex.match_simple ("https://demo.bigbluebutton.org/rooms/.+/join", this.link)) {
          if (this.implementation.type != typeof (Greenlight3LauncherImplementation)) {
            this.implementation = new Greenlight3LauncherImplementation (this);
          }
        } else if (Regex.match_simple ("https://meet.gnome.org/.+", this.link)) {
          if (this.implementation.type != typeof (Greenlight2LauncherImplementation)) {
            this.implementation = new Greenlight2LauncherImplementation (this);
          }
        } else if (Regex.match_simple ("https://.+\\.test", this.link)) {
          if (this.implementation.type != typeof (DemoLauncherImplementation)) {
            this.implementation = new DemoLauncherImplementation (this);
          }
        } else {
          this.link_is_valid = false;
          return;
        }

        this.link_is_valid = true;
      });
    }

    private LauncherImplementation implementation;

    public string username { get; set; default = ""; }
    public string password { get; set; default = ""; }

    internal Soup.Session session;

    public string link { get; set; }
    public bool link_is_valid { get; private set; default = false; }

    public string? room_name { get; set; }

    public async bool start_meeting () throws MeetingError {
      return yield this.implementation.start_meeting ();
    }

    public async bool room_exists_already () {
      try {
        return yield this.implementation.room_exists_already ();
      } catch (Error e) {
        return false;
      }
    }

    public async Connection join_meeting () throws MeetingError {
      return yield this.implementation.join_meeting ();
    }
  }

  private abstract class LauncherImplementation {
    public Launcher launcher;
    public Type type;

    protected LauncherImplementation (Launcher launcher, Type type) {
      this.launcher = launcher;
      this.type = type;
    }

    public abstract async bool start_meeting () throws MeetingError;
    public abstract async Connection join_meeting () throws MeetingError;
    public abstract async bool room_exists_already () throws MeetingError;

    internal async Connection finish_join (Uri uri, Bytes content) throws MeetingError {
      string request = uri.get_query ();
      string session_token = request.split ("=")[1];

      Json.Object? settings = null;
      uint length = content.length;
      string content_string = ((string) Bytes.unref_to_data (content)).substring (0, length);
      // extract meteor runtime config from html
      string raw = content_string.split ("__meteor_runtime_config__")[1].split ("\"")[1];
      print ("\n"+Uri.unescape_string (raw)+"\n");
      try {
        settings = Json.from_string (Uri.unescape_string (raw)).get_object ().get_object_member ("PUBLIC_SETTINGS");
      } catch (Error e) {
        critical (@"Failed to parse Meteor settings: $(e.message)\n");
        assert_not_reached ();
      }

      string address = Uri.join (UriFlags.NONE, "https", null, uri.get_host (), 443,
                                 "/bigbluebutton/api/enter", "sessionToken=" + session_token, null);
      var message = new Soup.Message ("GET", address);
      Bytes data = null;
      bool error = false;
      try {
        data = yield this.launcher.session.send_and_read_async (message, Soup.MessagePriority.NORMAL, null);
      } catch (Error e) {
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECTION ("error while connecting");
      }

      Json.Node? node = null;
      length = data.length;
      debug (@"$length");
      try {
        node = Json.from_string ((string) data.get_data ()[0:length] + "\0");
      } catch (Error e) {
        debug (e.message);
        debug (((string) Bytes.unref_to_data (data)).substring (0, length));
        error = true;
      }
      if (error) {
        throw new MeetingError.CONNECTION ("error while connecting");
      }
      Json.Object obj = node.get_object ();
      obj = obj.get_object_member ("response");
      string meeting_id = obj.get_string_member ("meetingID");
      string extern_id = obj.get_string_member ("externUserID");
      string intern_id = obj.get_string_member ("internalUserID");
      string auth_token = obj.get_string_member ("authToken");

      var connection = new Connection (this.launcher.session, message.uri.get_host (), meeting_id, extern_id, intern_id, auth_token, session_token, settings);
      return connection;
    }
  }
}

