// SPDX-FileCopyrightText: 2022 Lorenz Wildberg
//
// SPDX-License-Identifier: GPL-3.0-or-later

namespace MeetingPoint {
  // FIXME sealed
  private class ViewerVideoStream : RecieveStream {

    internal string camera_id;
    internal Gst.Element paintable_sink;

    internal ViewerVideoStream (Connection connection, string camera_id) {
      base (connection, StreamType.VIDEO, "viewer", camera_id);
      this.camera_id = camera_id;

      var queue = Gst.ElementFactory.make ("queue", "queue");
      var convert = Gst.ElementFactory.make ("videoconvert", "convert");
      this.paintable_sink = Gst.ElementFactory.make ("gtk4paintablesink", "paintable_sink");
      this.add (queue);
      this.add (convert);
      this.add (this.paintable_sink);
      queue.link (convert);
      convert.link (this.paintable_sink);

      this.decode_bin.pad_added.connect ((element, pad) => {
        if (!pad.has_current_caps ()) {
          return;
        }
        Gst.Caps? caps = pad.get_current_caps ();
        if (caps == null || caps.get_size () < 1) {
          return;
        }
        unowned Gst.Structure str = caps.get_structure (0);
        print (str.get_name () + "\n");

        pad.link (queue.get_static_pad ("sink"));
      });
    }

    internal override async Json.Builder add_fields_for_start (Json.Builder b) {
      Json.Builder builder = yield base.add_fields_for_start (b);

      add_string_member (builder, "cameraId", this.camera_id);
      builder.set_member_name("bitrate");
      builder.add_int_value (100);
      builder.set_member_name ("record");
      builder.add_boolean_value (true);
      return builder;
    }

    internal override void add_fields_for_subscriber_answer (ref Json.Builder builder) {
      base.add_fields_for_subscriber_answer (ref builder);

      add_string_member (builder, "cameraId", this.camera_id);
    }

    internal override void state_changed (Gst.State oldstate, Gst.State newstate, Gst.State pending) {
      if (oldstate == Gst.State.PAUSED && newstate == Gst.State.READY) {
        var builder = new Json.Builder ();
        builder.begin_object ();
        add_string_member (builder, "id", "stop");
        add_string_member (builder, "type", "video");
        add_string_member (builder, "cameraId", this.camera_id);
        add_string_member (builder, "role", this.role_string);
        builder.end_object ();
        this.connection.stream_manager.send_message (builder.get_root ());
      }
    }
  }

  private void add_string_member (Json.Builder builder, string member_name, string val) {
    builder.set_member_name (member_name);
    builder.add_string_value (val);
  }
}


